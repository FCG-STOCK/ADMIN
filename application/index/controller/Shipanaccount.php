<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;

class Shipanaccount extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
  	public function qwe(){
        $shipanstock=new Stock();
        $shipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
        foreach($shipan_account as $k=>$v){

            //查询账户的资金情况
            $success=$shipanstock->querydata($v['ip'],$v['version'],$v['yybID'],$v['zhanghao'],$v['zhanghao'],$v['mima'],$v['port'],$v['txmima'],2);
            $success=json_decode($success,true);
echo "<pre>";
print_r($success);
echo "</pre>";die;
            if($success['data1']['资金帐号']){
                $shipan_account[$k]['balance']=$success['data1']['可用资金'];
                $shipan_account[$k]['allbalance']=$success['data1']['总资产'];
                $shipan_account[$k]['zijinbalance']=$success['data1']['资金余额'];
            }
        }
        $this->assign('list',$shipan_account);
      	
        return $this->fetch();
    }
    public function index(){
        $shipanstock=new Stock();
        $shipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
        foreach($shipan_account as $k=>$v){

            //查询账户的资金情况
            $success=$shipanstock->querydata($v['ip'],$v['version'],$v['yybID'],$v['zhanghao'],$v['zhanghao'],$v['mima'],$v['port'],$v['txmima'],0);
            $success=json_decode($success,true);
            if(array_key_exists('保留信息',$success)){
                $shipan_account[$k]['balance']=0;
                $shipan_account[$k]['allbalance']=0;
                $shipan_account[$k]['zijinbalance']=0;
            }else{
                $shipan_account[$k]['balance']=$success['data1']['可用资金'];
                $shipan_account[$k]['allbalance']=$success['data1']['总资产'];
                $shipan_account[$k]['zijinbalance']=$success['data1']['资金余额'];
            }
        }
        $this->assign('list',$shipan_account);
      	
        return $this->fetch();
    }
    public function add(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //查询账号名是否重复
            $shipan_account=Db::name('shipan_account')->where(['is_del'=>1,'zhanghao'=>$info['account']])->find();
            if($shipan_account){$this->error('账号名已重复');}

            $data['zhanghao']=$info['account'];
            $data['mima']=$info['pwd'];
            $data['name']=$info['username'];
            $data['ip']=$info['ip'];
            $data['version']=$info['version'];
            $data['yybID']=$info['yybID'];
            $data['port']=$info['port'];
            $data['txmima']=$info['txmima'];
            $data['is_del']=1;
            $data['create_time']=time();

            //验证规则
            $rule = [
                'zhanghao' =>  'require',
                'mima' =>  'require',
                'name' =>  'require',
            ];
            $msg = [
                'zhanghao' =>  '账号不能为空',
                'mima' =>  '密码不能为空',
                'name' =>  '名字不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            };

            //查询股东代码
            $shipanstock=new Stock();
            $success=$shipanstock->querydata($data['ip'],$data['version'],$data['yybID'],$data['zhanghao'],$data['zhanghao'],$data['mima'],$data['port'],$data['txmima'],5);
          

            $success=json_decode($success,true);
            if($success['data1']['资金帐号']){
                if($success['data1']['帐号类别']==1){
                    $data['shholder']=$success['data1']['股东代码'];
                    $data['szholder']=$success['data2']['股东代码'];
                }elseif($success['data2']['帐号类别']==1){
                    $data['shholder']=$success['data2']['股东代码'];
                    $data['szholder']=$success['data1']['股东代码'];
                }
            }else{
                $this->error('获取股东代码失败');
            }

            $res = Db::name('shipan_account')->insert($data);
            if($res){
                $this->success('添加成功','shipanaccount/index');
            }else{
                $this->error('添加失败');
            }
        }
        return $this->fetch();
    }
    public function update(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['zhanghao']=$info['account'];
            $data['name']=$info['username'];
            $data['mima']=$info['pwd'];
            $data['ip']=$info['ip'];
            $data['version']=$info['version'];
            $data['yybID']=$info['yybID'];
            $data['port']=$info['port'];
            $data['txmima']=$info['txmima'];

            //验证规则
            $rule = [
                'zhanghao' =>  'require',
                'mima' =>  'require',
                'name' =>  'require',
            ];
            $msg = [
                'zhanghao' =>  '账号不能为空',
                'mima' =>  '密码不能为空',
                'name' =>  '名字不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //查询股东代码
            $shipanstock=new Stock();
            $success=$shipanstock->querydata($data['ip'],$data['version'],$data['yybID'],$data['zhanghao'],$data['zhanghao'],$data['mima'],$data['port'],$data['txmima'],5);
            $success=json_decode($success,true);
            if($success['data1']['资金帐号']){
                if($success['data1']['帐号类别']==1){
                    $data['shholder']=$success['data1']['股东代码'];
                    $data['szholder']=$success['data2']['股东代码'];
                }elseif($success['data2']['帐号类别']==1){
                    $data['shholder']=$success['data2']['股东代码'];
                    $data['szholder']=$success['data1']['股东代码'];
                }
            }else{
                $this->error('获取股东代码失败');
            }

            $res = Db::name('shipan_account')->where(['id'=>$info['id']])->update($data);
            if($res){
                $this->success('修改成功','shipanaccount/index');
            }else{
                $this->error('修改失败');
            }
        }
        $id=input('id');
        $shipan_account=Db::name('shipan_account')->where(['id'=>$id])->find();
        $this->assign('shipan_account',$shipan_account);
        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        Db::name('shipan_account')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('禁用成功', 'shipanaccount/index');
    }

}
