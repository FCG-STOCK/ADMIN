<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Strategyanalog extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);

        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }

    public function index() {
        $list = Db::name('strategy_analog')->where(['is_del'=>1])->select();
      	if($list){
            foreach($list as $k=>$v){
                $list[$k]['create_time']=date('Y-m-d H:i:s');
            }
        }
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function add(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param();

            $data['account']=$info['account'];
            $data['stock_name']=$info['stock_name'];
            $data['stock_code']=$info['stock_code'];
            $data['stock_number']=$info['stock_number'];
            $data['is_del']=1;
            $data['create_time']=time();

            //验证规则
            $rule = [
                'account' =>  'require',
                'stock_name' =>  'require',
                'stock_code' =>  'require',
                'stock_number' =>  'require'
            ];
            $msg = [
                'account' =>  '账号不能为空',
                'stock_name' =>  '股票名字不能为空',
                'stock_code' =>  '股票代码不能为空',
                'stock_number' =>  '股数不能为空'
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)) {
                $this->error($validate->getError());
            }

            $data['buy_price']=explode('~',iconv("gb2312", "utf-8//IGNORE",file_get_contents('http://qt.gtimg.cn/q='.$info['stock_code'])))[3];
            
            $res = Db::name('strategy_analog')->insert($data);
            if($res){
                //返回到当前的新闻分类页面
                $this->success('添加成功',url('strategyanalog/index'));
            }else{
                $this->error('添加失败');
            }
        }
        return $this->fetch();
    }

    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param();

            $data['account']=$info['account'];
            $data['stock_name']=$info['stock_name'];
            $data['stock_code']=$info['stock_code'];
            $data['stock_number']=$info['stock_number'];
            if($info['buy_price']){
                $data['buy_price']=$info['buy_price'];
            }
            
            //验证规则
            $rule = [
                'account' =>  'require',
                'stock_name' =>  'require',
                'stock_code' =>  'require',
                'stock_number' =>  'require'
            ];
            $msg = [
                'account' =>  '账号不能为空',
                'stock_name' =>  '股票名字不能为空',
                'stock_code' =>  '股票代码不能为空',
                'stock_number' =>  '股数不能为空'
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)) {
                $this->error($validate->getError());
            }
            
            Db::name('strategy_analog')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功',url('strategyanalog/index'));
        }
        $id=input('id');
        $strategy=Db::name('strategy_analog')->where(['id'=>$id])->find();
        $this->assign('strategy',$strategy);
        return $this->fetch();
    }

    public function del(){
        $id=input('id');
        Db::name('strategy_analog')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('删除成功',url('strategyanalog/index'));
    }

}
