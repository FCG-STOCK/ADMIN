function register(){
  		var phone = $("#username").val();
        if(phone=='' || !(/^1[3456789]\d{9}$/.test(phone))){
           $("#tishi").css({"opacity":"1","z-index":"999"});
         	 $("#tishi").html("请输入正确的手机号码!");
          	setTimeout(function () {$("#tishi").css("opacity","0");  }, 2000);//延迟1秒后跳转
            return;
        }
  
  		if($("#passworde").val().length<6){
            $("#tibom").css({"opacity":"1","z-index":"999"});
            $("#tibom").html("密码长度不能小于6位!");
           	setTimeout(function () {$("#tibom").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
            return;
        }
  
  		if($("#verify").val()==''){
            $("#titop").css({"opacity":"1","z-index":"999"});
            $("#titop").html("请输入验证码!");
           	setTimeout(function () {$("#titop").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
            return;
        }

        if($("#inviter_tel").val()==''){
            $("#titop").css({"opacity":"1","z-index":"999"});
            $("#titop").html("请输入邀请人手机号!");
            setTimeout(function () {$("#titop").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
            return;
        }
  
  
  		if(!($("input[type='checkbox']").is(':checked'))){
        	 $(".tip4").css({"opacity":"1","z-index":"999"});
           	 $(".tip4").html("注册需同意相关协议"); 
         	 setTimeout(function () {$(".tip4").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
          	return;
        }

        $(function () {               
                    $.ajax({
                        url: "/API/index/register",
                        data: { Account: $("#username").val(),
                                yzm: $("#verify").val(),
                                pwd: $("#passworde").val(),
                                inviter:$("#inviter_tel").val()
                                  },
                        dataType: "json",
                        method:"POST",
                        success: function (data) {
                            var dataObj = JSON.parse(data);
                            if(dataObj.status==1){
                                window.location.href = 'login.html';
                            }else{
                              	if(dataObj.status==1100){
                                	 
                                    $("#titop").css({"opacity":"1","z-index":"999"});
                                    $("#titop").html(dataObj.msg);
                                    setTimeout(function () {$("#titop").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
                                    
                                }
                              	if(dataObj.status==1101){
                                	$("#titop").css({"opacity":"1","z-index":"999"});
                                    $("#titop").html(dataObj.msg);
                                    setTimeout(function () {$("#titop").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转                               
                                }
                              	if(dataObj.status==1102){
                                	$("#titop").css({"opacity":"1","z-index":"999"});
                                    $("#titop").html(dataObj.msg);
                                    setTimeout(function () {$("#titop").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转                             
                                }
                              	if(dataObj.status==1103){
                                    $("#tishi").css({"opacity":"1","z-index":"999"});
                                     $("#tishi").html(dataObj.msg);
                                    setTimeout(function () {$("#tishi").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转
                                      
                                }
                              	if(dataObj.status==1104){
                                	$("#tipyqm").css({"opacity":"1","z-index":"999"});
                                     $("#tipyqm").html(dataObj.msg);
                                    setTimeout(function () {$("#tipyqm").css({"opacity":"0","z-index":"-99"});  }, 2000);//延迟1秒后跳转                                
                                }
     
                            }                            

                        },
                        error:function(){
                            alert("注册失败");
                        }
                    })
                                         
                })        
    }   
                   
function yzm(){
  	var phone = $("#username").val();
  	if(phone=='' || !(/^1[3456789]\d{9}$/.test(phone))){
      	$(".tip").css("display","block");
        $(".tip").css("color","red");
      	$(".tip").css("margin","-3%");
        $(".tip").html("请输入正确的手机号");
      	return;
    }
	
    $(function () {               
                $.ajax({
                    url: "/API/index/sendMessage",
                    data: { mobile: $("#username").val()
                              },
                    dataType: "json",
                    method:"POST",
                    success: function (data) {
                  
                      var dataObj = JSON.parse(data);
                      if(dataObj.status==1){
                          // alert("发送成功");
                          var count = 60;
                          var time = setInterval(function(){
                              count--;
                              $("#seconds").html("("+ count +")");
                              $("#seconds").removeAttr("onclick");
                              if(count==0){
                                  clearInterval(time);
                                  $("#seconds").html("重发");
                                  $("#seconds").attr("onclick","yzm()");
                              }
                          },1000)
                      }else{
                          if(dataObj.status==1103){
                              $("#tishi").css({"opacity":"1","z-index":"999"});
                               $("#tishi").html(dataObj.msg);
                              setTimeout(function () {$("#tishi").css("opacity","0");  }, 2000);//延迟1秒后跳转             
                          } 
                      }             
                	},

                    error:function(){
                        alert("手机号不正确");
                    }
                })                                         
            })        
    }