<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Admin extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();

        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }

        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    //会员
    public function index(){
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }
        $info = Request::instance()->param();
        //搜索中的是否实盘查询
        if(array_key_exists('is_real_disk',$info)&&$info['is_real_disk']){
            $where['is_real_disk'] = $info['is_real_disk'];
            $this->assign('is_real_disked',$info['is_real_disk']);
        }

        $list = Db::name('admin')->where($where)->select();

        //所有策略
        $strategy=Db::name('strategy')
            ->field('account,status,stock_code,stock_number,credit,credit_add,true_getmoney,buy_price,sell_poundage')
            ->where(['is_cancel_order'=>1])
            ->select();
        //获取当前单价
        $strategyarr=[];
        $cishu=ceil(count($strategy)/500);
        for($i=1;$i<=$cishu;$i++){
            foreach($strategy as $k=>$v){
                if($k<500*$i&&$k>=500*($i-1)){
                    $strategyarr[$i][]=$v;
                }
            }
        }
        foreach($strategyarr as $k=>$v){
            $str='';
            foreach($v as $kk=>$vv){
                $str.=$vv['stock_code'].',';
            }
            $str=substr($str,0,-1);
            $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
            $arr=explode(';',$result);
            unset($arr[count($arr)-1]);
            foreach($arr as $kk=>$vv){
                $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
            }
        }
      	foreach($strategyarr as $k=>$v){
            foreach($v as $kk=>$vv){
                $strategy[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
            }
        }
      
      	
        //每个策略的浮动盈亏和平仓盈亏
        foreach($strategy as $k=>$v){
            if($v['status']==3||$v['status']==4){
                $strategy[$k]['yingkui']=($v['true_getmoney']-$v['credit']-$v['credit_add']+$v['sell_poundage'])/$chengshu;;
            }else{
                $strategy[$k]['yingkui']=$v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'];
            }
        }

        //所有订单
        $trade=Db::name('trade')
            ->field('account,trade_price,trade_type,trade_status')
            ->where(['trade_type'=>['in',['充值','后台充值','支付宝充值','支付宝转账','微信转账','银行卡转账','除权派息','提现']],'is_del'=>1])
            ->select();
        //把每人的充值提现金额和所有余额添加，把策略的持仓平仓
        foreach($list as $k=>$v){
            $list[$k]['yingkui']=0;
            $list[$k]['chongzhi']=0;
            $list[$k]['tixian']=0;
            foreach($trade as $kk=>$vv){
                if($v['account']==$vv['account']){
                    if($vv['trade_type']=='充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='后台充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='支付宝充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='支付宝转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='微信转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='除权派息'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='银行卡转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='提现'){
                        if($vv['trade_status']==1||$vv['trade_status']==3||$vv['trade_status']==5){
                            $list[$k]['tixian']+=$vv['trade_price'];
                        }
                    }
                }
            }
            $list[$k]['yingkui']=($v['balance']+$v['tactics_balance']+$v['frozen_money'])-($list[$k]['chongzhi']-$list[$k]['tixian']);
            $list[$k]['chicangyingkui']=0;
            $list[$k]['pingcangyingkui']=0;
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    if($vv['status']==1||$vv['status']==2){
                        $list[$k]['chicangyingkui']+=$vv['yingkui'];
                    }else{
                        $list[$k]['pingcangyingkui']+=$vv['yingkui'];
                    }
                }
            }
        }

        foreach($list as $k=>$v){
            $list[$k]['yingkui']=round($v['yingkui'],2);
            $list[$k]['chicangyingkui']=round($v['chicangyingkui'],2);
            $list[$k]['pingcangyingkui']=round($v['pingcangyingkui'],2);
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d',$v['lastlogin_time']);
            if($v['is_del']==1){
                $list[$k]['is_del']='正常';
            }else{
                $list[$k]['is_del']='禁用';
            }
            if($v['is_true']==1){
                $list[$k]['is_true']='已认证';
            }elseif($v['is_true']==2){
                $list[$k]['is_true']='认证失败';
            }elseif($v['is_true']==3){
                $list[$k]['is_true']='待审核';
            }else{
                $list[$k]['is_true']='未提交';
            }
            //推广人员个数
            $list[$k]['extend'] = Db::name('admin')->where(['inviter'=>$v['account'],'is_del'=>1])->count();

            //LRSS 2019-2-28 ADD 获取邀请人的姓名
            if($v['marketer_user']){
                //查询marketer_user表
                $list[$k]['inviter_name'] = Db::name('marketer_user')->where(['id'=>$v['marketer_user']])->value('username');
            }else{
                //根据邀请人手机号查询admin表
                $list[$k]['inviter_name'] = Db::name('admin')->where(['account'=>$v['marketer_user']])->value('true_name');
            }
        }
        $this->assign('list',$list);

        $this->assign('marketer_user',cookie('marketer_user'));
        return $this->fetch();
    }
    //余额不足的会员
    public function balanceadmin(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }
        $list = Db::name('admin')->where($where)->where(['tactics_balance'=>['lt',0],'is_del'=>1])->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d',$v['lastlogin_time']);
            if($v['is_del']==1){
                $list[$k]['is_del']='正常';
            }else{
                $list[$k]['is_del']='禁用';
            }
            //推广人员个数
            $list[$k]['extend'] = Db::name('admin')->where(['inviter'=>$v['account'],'is_del'=>1])->count();
        }
        $this->assign('list',$list);
        return $this->fetch();
    }

    //账户余额异常的会员
    public function abnormaladmin(){
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }

        $list = Db::name('admin')->where($where)->where(['is_del'=>1])->select();
        foreach($list as $k=>$v){
            $chongzhi=0;
            $tixian=0;
            $trade=Db::name('trade')->where(['account'=>$v['account'],'trade_type'=>['in',['充值','支付宝转账','微信转账','银行卡转账','后台充值','除权派息','提现']],'trade_status'=>1,'is_del'=>1])->select();
            foreach($trade as $kk=>$vv){
                if($vv['trade_type']=='充值'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='后台充值'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='支付宝转账'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='微信转账'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='银行卡转账'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='除权派息'){
                    $chongzhi+=$vv['trade_price'];
                }elseif($vv['trade_type']=='提现'){
                    $tixian+=$vv['trade_price'];
                }
            }
            $list[$k]['chongzhi']=$chongzhi;
            $list[$k]['tixian']=$tixian;

            $tixianzhong=0;
            $ti=Db::name('trade')->where(['account'=>$v['account'],'trade_status'=>5,'is_del'=>1])->select();
            foreach($ti as $kk=>$vv){
                $tixianzhong+=$vv['trade_price'];
            }
            $list[$k]['tixianzhong']=$tixianzhong;

            $shouxufei=0;
            $diyanfei=0;
            $yingli=0;
            $credit_add_money=0;
            $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['account'=>$v['account']])->select();
            foreach($strategy as $kk=>$vv){
                $shouxufei+=$vv['all_poundage'];
                $diyanfei+=$vv['defer_value'];
                //已平仓的按照平仓价格计算
                if($vv['status']==3||$vv['status']==4){
                    //策略盈亏(收益)
                    if($vv['sell_price']*$vv['stock_number']-$vv['market_value']>0){
                        //赢利分配
                        $yingli += ($vv['sell_price']*$vv['stock_number']-$vv['market_value'])*$chengshu;
                    }else{
                        //赢利分配
                        $yingli += ($vv['sell_price']*$vv['stock_number']-$vv['market_value']);
                    }
                }else{
                    $credit_add_money += $vv['credit_add'];
                }
            }
            $list[$k]['shouxufei']=$shouxufei;
            $list[$k]['diyanfei']=$diyanfei;
            $list[$k]['yingli']=round($yingli,2);
            $list[$k]['credit_add']=$credit_add_money;
        }

        foreach($list as $k=>$v){
            $jisuan=round($v['chongzhi']-$v['tixian']-$v['tixianzhong']-$v['shouxufei']-$v['diyanfei']+$v['yingli'],2);
            $yue=round($v['balance']+$v['tactics_balance']+$v['frozen_money'],2);
            $list[$k]['chazhi']=round($jisuan-$yue,2);
            if(($jisuan-$yue)<0.01&&($jisuan-$yue)>-0.01){
                unset($list[$k]);
            }
        }

        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d H:i:s',$v['lastlogin_time']);
        }
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['account']=$info['account'];
            //有密码则修改数据库
            if($info['pwd']){
                $data['pwd']=md5($info['pwd']);
            }
            if($info['trade_pwd']){
                $data['trade_pwd']=md5($info['trade_pwd']);
            }
            $data['is_real_disk']=$info['is_real_disk'];
            if($info['is_real_disk']==1){
                $data['shipan_account']=$info['shipan_account'];
            }else{
                $data['shipan_account']='';
            }
            if(array_key_exists('is_true',$info)&&$info['is_true']){
                $data['is_true']=$info['is_true'];
            }
            $data['is_open_ranking']=$info['is_open_ranking'];
            $data['card_id']=$info['card_id'];
            $data['nick_name']=$info['nick_name'];
            $data['true_name']=$info['true_name'];

            //验证规则
            if(!$data['account']){
                $this->error('账号不能为空');
            }

            $rule = [
                'account' =>  '^1[3456789]\d{9}$',
                'nick_name' =>  'require',
                'inviter' =>  'number',
            ];
            $msg = [
                'account' =>  '账号格式不正确',
                'nick_name' =>  '昵称不能为空',
                'inviter' =>  '邀请人必须为数字',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //用户头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
//                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            //判断工作人员表中是否有此分销商工作人员编号
            $marketuser=Db::name('marketer_user')->where(['id'=>$info['inviter'],'is_del'=>1])->find();
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['inviter'],'is_del'=>1])->find();
            if($marketuser){
                //最近一次受此工作人员推广的用户  例8010012
                $list=Db::name('admin')->field('id')->where(['marketer_user'=>$info['inviter'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']=substr($id,0,6).(substr($id,6)+1);
                }else{
                    $data['id']=$info['inviter'].'1';
                }
                //添加分销商id和工作人员id
                $data['marketer_id']=substr($info['inviter'],0,3);
                $data['marketer_user']=$info['inviter'];
                $data['inviter']=$info['inviter'];

                //如果修改邀请人至分销商中，实盘信息则改成分销商的实盘信息
                $marketer_user=Db::name('marketer')->where(['id'=>$data['marketer_id']])->find();
                $data['is_real_disk']=$marketer_user['is_real_disk'];
                $data['shipan_account']=$marketer_user['shipan_account'];
            }elseif($inviter){
                //最近一次没有分销商的普通用户  例0000002
                $list=Db::name('admin')->field('id')->where(['id'=>['like','000000'.'%'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']='000000'.($id+1);
                }else{
                    $data['id']='0000001';
                }
                $data['marketer_id']=0;
                $data['marketer_user']=0;
                //添加手机号到邀请人
                $data['inviter']=$info['inviter'];

                //如果修改邀请人至普通用户中，实盘信息则改成不走实盘
                $data['is_real_disk']=2;
                $data['shipan_account']='';
            }
			
          	$admin=Db::name('admin')->where(['id'=>$info['id'],'is_del'=>1])->find();
          	if($admin['is_true']==3){push($admin['account'],'恭喜您，您的实名认证信息已经审核通过！',1);}
          
            Db::name('admin')->where(['id'=>$info['id'],'is_del'=>1])->update($data);

            if($info['shenhe']==1){
                $this->success('提交成功', 'admin/index');
            }elseif($info['shenhe']==2){
                $this->success('提交成功', 'admin/enticationadmin');
            }
        }
        $shenhe=input('shenhe');
        $this->assign('shenhe',$shenhe);

        $id=input('id');
        $user=Db::name('admin')->where(['id'=>$id,'is_del'=>1])->find();
        $this->assign('user',$user);

        $shipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
        $this->assign('shipan_account',$shipan_account);

        return $this->fetch();
    }
    //禁用会员
    public function del(){
        $id=input('id');
        Db::name('admin')->where(['id'=>$id,'is_del'=>1])->update(['is_del'=>2]);
        $this->success('禁用成功', 'admin/index');
    }
    //启用会员
    public function enableadmin(){
        $id=input('id');
        Db::name('admin')->where(['id'=>$id,'is_del'=>2])->update(['is_del'=>1]);
        $this->success('启用成功', 'admin/index');
    }
    //待进行实名认证审核的会员
    public function enticationadmin(){
        $list = Db::name('admin')->where(['is_true'=>3,'is_del'=>1])->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d',$v['lastlogin_time']);
            if($v['is_del']==1){
                $list[$k]['is_del']='正常';
            }else{
                $list[$k]['is_del']='禁用';
            }
        }
        $this->assign('list',$list);
        return $this->fetch();
    }
    //邀请人（搜索工作人员）
    public function searchMarketerUser(){
        $info=Request::instance()->param();
        if(!$info['content']){
            return ['status'=>0,'msg'=>'请填写邀请人'];
        }
        $list=Db::name('marketer_user')->where(['id'=>['like','%'.$info['content'].'%']])->select();
        $count=count($list);
        return ['count'=>$count,'list'=>$list,'status'=>1,'msg'=>'搜索成功'];
    }
    //银行卡页面
    public function bankindex(){
        $list = Db::name('bankcart')->where(['account'=>input('account'),'bank_require'=>1,'is_del'=>1])->find();
        if($list){
            $list['create_time'] = date('Y-m-d H:i:s', $list['create_time']);
        }
        $this->assign('list',$list);
        $this->assign('account',input('account'));
        return $this->fetch();
    }
    //更新银行卡页面
    public function updbankindex(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            if(!$info['bank_num']){$this->error('银行卡号不能为空');}

            $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_num'].'&cardBinCheck=true');
            if(strpos($str,'errorCodes')){
                $this->error('卡号输入有误，请重新输入');
            }
          	$data=[];
            $banktype=explode('"',explode(':',$str)[2])[1];
            foreach(config('terminal') as $k => $v) {
                if($v['banknum']==$banktype){
                    $data['logo']=$v['logo'];
                    $data['background']=$v['background'];
                    $data['bank_name']=$v['name'];
                }
            }
          
          	if(!isset($data['background'])){
                $data['logo']=config('bank_default')['logo'];
                $data['background']=config('bank_default')['background'];
                $data['bank_name']='银行卡';
            }
            $data['account']=$info['account'];
          	$data['bank_num']=$info['bank_num'];
          	$data['bank_address']=$info['bank_address'];
            $data['card_type']=2;
            $data['bank_require']=1;
            $data['is_del']=1;
            $data['create_time']=time();

            //解绑卡
            if(array_key_exists('id',$info)&&$info['id']){
                Db::name('bankcart')->where(['id'=>$info['id']])->update(['is_del'=>2,'relieve_time'=>time()]);
            }
            Db::name('bankcart')->insert($data);
            $this->success('提交成功', url('admin/bankindex',['account'=>$info['account']]));
        }
        if(input('id')){$this->assign('id',input('id'));}
        $this->assign('account',input('account'));
        return $this->fetch();
    }

    //推广的人员的页面
    public function extendindex(){
        //获取查看的用户的手机号
        $account=Db::name('admin')->where(['id'=>input('id'),'is_del'=>1])->value('account');
        //根据邀请人来查询推广人数
        $list=Db::name('admin')->where(['inviter'=>$account,'is_del'=>1])->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->assign('list',$list);
        return $this->fetch();
    }
    //会员导出
    function exportAdmin(){
        $info = Request::instance()->param();

        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }
        $wh=[];
        if($info['type']==1){
            $wh=[];
        }elseif($info['type']==2){
            $wh['tactics_balance']=['lt',0];
        }

        //搜索中的是否实盘查询
        if(array_key_exists('is_real_disk',$info)&&$info['is_real_disk']){
            $where['is_real_disk'] = $info['is_real_disk'];
            $this->assign('is_real_disked',$info['is_real_disk']);
        }

        //获取用户基础信息
        $list = Db::name('admin')
            ->where($where)
            ->where($wh)
            ->where(['is_del'=>1])
            ->select();

        //所有策略
        $strategy=Db::name('strategy')
            ->field('account,status,stock_code,stock_number,credit,credit_add,true_getmoney,buy_price,all_poundage,sell_poundage,defer_value')
            ->where(['is_cancel_order'=>1])
            ->select();
        //获取当前单价
        $str='';
        foreach($strategy as $k=>$v){
            $str.=$v['stock_code'].',';
        }
        $str=substr($str,0,-1);
        $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
        $arr=explode(';',$result);
        unset($arr[count($arr)-1]);
        foreach($arr as $k=>$v){
            $strategy[$k]['nowprice']=explode(',',$v)[3];
        }
        //每个策略的浮动盈亏和平仓盈亏
        foreach($strategy as $k=>$v){
            if($v['status']==3||$v['status']==4){
                $strategy[$k]['yingkui']=($v['true_getmoney']-$v['credit']-$v['credit_add']+$v['sell_poundage'])/$chengshu;
            }else{
                $strategy[$k]['yingkui']=$v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'];
            }
        }

        //所有订单
        $trade=Db::name('trade')
            ->where(['trade_type'=>['in',['充值','后台充值','支付宝充值','支付宝转账','微信转账','银行卡转账','除权派息','提现']],'is_del'=>1])
            ->field('account,trade_price,trade_type,trade_status')
            ->select();
        //把每人的充值提现金额和所有余额添加，把策略的持仓平仓
        foreach($list as $k=>$v){
            $list[$k]['chongzhi']=0;
            $list[$k]['tixian']=0;
            foreach($trade as $kk=>$vv){
                if($v['account']==$vv['account']){
                    if($vv['trade_type']=='充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='后台充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='支付宝充值'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='支付宝转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='除权派息'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='微信转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='银行卡转账'&&$vv['trade_status']==1){
                        $list[$k]['chongzhi']+=$vv['trade_price'];
                    }elseif($vv['trade_type']=='提现'){
                        if($vv['trade_status']==1||$vv['trade_status']==3||$vv['trade_status']==5){
                            $list[$k]['tixian']+=$vv['trade_price'];
                        }
                    }
                }
            }
            $list[$k]['yingkui']=($v['balance']+$v['tactics_balance']+$v['frozen_money'])-($list[$k]['chongzhi']-$list[$k]['tixian']);
            $list[$k]['chicangyingkui']=0;
            $list[$k]['pingcangyingkui']=0;
            $list[$k]['shouxufei']=0;
            $list[$k]['diyanfei']=0;
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    if($vv['status']==1||$vv['status']==2){
                        $list[$k]['chicangyingkui']+=$vv['yingkui'];
                    }else{
                        $list[$k]['pingcangyingkui']+=$vv['yingkui'];
                    }
                    $list[$k]['shouxufei']+=$vv['all_poundage'];
                    $list[$k]['diyanfei']+=$vv['defer_value'];
                }
            }
            //异常金额=充值-提现-手续费-递延费+所有策略盈亏-所有余额
            $list[$k]['chazhi']=
                ($list[$k]['chongzhi']-$list[$k]['tixian'])
                -$list[$k]['shouxufei']-$list[$k]['diyanfei']
                +($list[$k]['chicangyingkui']+$list[$k]['pingcangyingkui'])
                -($v['balance']+$v['tactics_balance']+$v['frozen_money']);
        }
        foreach($list as $k=>$v){
            $list[$k]['yingkui']=round($v['yingkui'],2);
            $list[$k]['chicangyingkui']=round($v['chicangyingkui'],2);
            $list[$k]['pingcangyingkui']=round($v['pingcangyingkui'],2);
        }


        //排除掉金额正常的用户
        if($info['type']==3){
            foreach($list as $k=>$v){
                if($v['chazhi']<0.01&&$v['chazhi']>-0.01){
                    unset($list[$k]);
                }
            }
        }

        foreach($list as $k=>$v){
            //推广人员个数
            $list[$k]['extend'] = Db::name('admin')->where(['inviter'=>$v['account'],'is_del'=>1])->count();
        }

        //列表中的字段
        $last=[];
        foreach($list as $k=>$v){
            $last[$k]['id']=$v['id'];
            $last[$k]['account']=$v['account'];
            $last[$k]['balance']=$v['balance'];
            $last[$k]['tactics_balance']=$v['tactics_balance'];
            $last[$k]['yingkui']=$v['yingkui'];
            $last[$k]['chicangyingkui']=$v['chicangyingkui'];
            $last[$k]['pingcangyingkui']=$v['pingcangyingkui'];
            $last[$k]['true_name']=$v['true_name'];
            $last[$k]['card_id']=$v['card_id'];
            $last[$k]['is_true']=$v['is_true'];
            $last[$k]['inviter']=$v['inviter'];
            $last[$k]['is_del']=$v['is_del'];
            $last[$k]['extend']=$v['extend'];
            $last[$k]['create_time']=$v['create_time'];
            $last[$k]['lastlogin_time']=$v['lastlogin_time'];
        }

        //筛选条件
        $shuzu=[];
        if(array_key_exists('content',$info)&&$info['content']){
            //所有会员中筛选要导出的数据
            foreach ($last as $k => $v) {
                foreach ($v as $kk => $vv) {
                    if(strpos($vv,$info['content'])||strpos($vv,$info['content'])===0){$shuzu[$k]=$v;};
                }
            }
        }else{
            $shuzu=$list;
        }

        //在获取到的列表中选择搜索框的like
        $zuihou=[];
        foreach($list as $k=>$v){
            foreach($shuzu as $kk=>$vv){
                if($k==$kk){
                    $zuihou[]=$v;
                }
            }
        }

        foreach($zuihou as $k=>$v){
            //是否进入实盘
            if($v['is_real_disk']==1){
                $zuihou[$k]['is_real_disk']='进入实盘';
            }elseif($v['is_real_disk']==2){
                $zuihou[$k]['is_real_disk']='不进入实盘';
            }
            //实盘信息是否开放
            if($v['is_open_ranking']==1){
                $zuihou[$k]['is_open_ranking']='开放';
            }elseif($v['is_open_ranking']==2){
                $zuihou[$k]['is_open_ranking']='不开放';
            }
            //是否实名认证
            if($v['is_true']==1){
                $list[$k]['is_true']='已认证';
            }elseif($v['is_true']==2){
                $list[$k]['is_true']='认证失败';
            }elseif($v['is_true']==3){
                $list[$k]['is_true']='待审核';
            }else{
                $list[$k]['is_true']='未提交';
            }
            //是否禁用
            if($v['is_del']==1){
                $zuihou[$k]['is_del']='正常';
            }else{
                $zuihou[$k]['is_del']='禁用';
            }
            //创建时间
            if($v['create_time']){
                $zuihou[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            }
            //最近登录时间
            if($v['lastlogin_time']){
                $zuihou[$k]['lastlogin_time']=date('Y-m-d H:i:s',$v['lastlogin_time']);
            }
        }

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($zuihou as $k => $v) {
            $zuihou[$k]['id']=' '.$v['id'];
            $zuihou[$k]['card_id']=$v['card_id'].' ';
        }
        $title='会员信息表';
        $cellName = array('ID', '用户账号', '登陆密码', '头像', '分销商id'
            , '分销商员工id', '极光id', '账户余额', '策略账户余额', '账号冻结金额'
            , '炒股大赛资金', '炒股大赛资金的冻结金额', '是否进入实盘', '进入实盘后使用的实盘账号'
            , '实盘信息是否开放', '身份证', '昵称', '真实姓名', '身份证正面'
            , '身份证背面', '是否实名认证', '交易密码', '邀请人', '是否禁用'
            , '创建时间', '最近登录时间', '总充值金额', '总提现金额', '盈亏'
            , '浮动盈亏', '平仓盈亏', '总手续费', '总递延费', '异常金额', '推广人员个数');
        daochu($title,$cellName,$zuihou);
    }

    //推送
    public function push($account='',$content='',$push_type=1,$true='') {
        $regid=Db::name('admin')->where(['account' => $account,'is_del'=>1])->value('regid');
        if($true||!$regid){
            //添加到推送记录表
            $data=[
                'push_type'=>$push_type,
                'account'=>$account,
                'regid'=>'',
                'content'=>$content,
                'status'=>1,
                'is_del'=>1,
                'create_time'=>time(),
            ];
            Db::name('push')->insert($data);
        }else{
            $client = new \JPush\Client(config('push')['app_key'],config('push')['master_secret']);
            $result=$client->push()
                ->setPlatform('all')
                ->addAlias(strval($regid))
                ->setNotificationAlert($content)
                ->send();
            //添加到推送记录表
            $data=[
                'push_type'=>$push_type,
                'account'=>$account,
                'regid'=>'',
                'content'=>$content,
                'is_del'=>1,
                'create_time'=>time(),
            ];
            if($result['http_code']==200){
                $data['status']=1;
            }else{
                $data['status']=2;
            }
            Db::name('push')->insert($data);
        }
    }



  
  
  
  	/**
     * 新手引导
     */
    public function guide()
    {
        $list = Db::name('guide')->where('is_del',1)->select();
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 新增新手引导
     */
    public function addGuide()
    {
        $info = Request::instance()->param(true);
        if (empty($info['state'])) {
            return $this->fetch();
        } else {
          	//后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            if(strpos($info['problem_detail'],$fonturl.'/public') || strpos($info['problem_detail'],$fonturl.'/public')===0){
            }else{
                $info['problem_detail'] = str_replace('/public',$fonturl.'/public',$info['problem_detail']);
            }

            $bool = Db::name('guide')->insert(['problem'=>$info['problem'],'problem_detail'=>$info['problem_detail']]);
            if ($bool) {$this->success('添加成功','/index/admin/guide');} else {$this->error('添加失败');}
        }
        
    }

    /**
     * 删除新手引导
     */
    public function guideDel()
    {
        $info = Request::instance()->param(true);

        $bool = Db::name('guide')->where('id',$info['id'])->update(['is_del'=>2]);
        if ($bool) {$this->success('删除成功','/index/admin/guide');} else {$this->error('删除失败');}
    }

    /**
     * 编辑新手引导问题
     */
    public function guideUpdate()
    {
        $info = Request::instance()->param(true);

        if (empty($info['state'])) {
            $list = Db::name('guide')->where('id',$info['id'])->find();
            $this->assign('list',$list);
          
            return $this->fetch();
        } else {
          	//后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            $data['problem'] = $info['problem'];
            if(strpos($info['problem_detail'],$fonturl.'/public') || strpos($info['problem_detail'],$fonturl.'/public')===0){
            }else{
                $data['problem_detail'] = str_replace('/public',$fonturl.'/public',$info['problem_detail']);
            }
          	
            Db::name('guide')->where('id',$info['id'])->update($data);
          	$this->success('修改成功','/index/Admin/guide');
        } 
    }


}
