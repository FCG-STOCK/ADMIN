<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Webspe extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();

        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);

        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
        //后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index(){
        $list = Db::name('web_spe')
            ->where(['is_del'=>1])
            ->select();
        foreach($list as $k=>$v){
            //轮播图状态
            if($v['is_del']==1){
                $list[$k]['is_del']='未禁用';
            }else{
                $list[$k]['is_del']='已禁用';
            }
            
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            //轮播图
            $file = request()->file('spe_img');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['spe_img']= $fonturl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            
            $data['is_del']=1;

            $data['spe_img']=$info['spe_img'];
            //验证规则
            $rule = [
                'spe_img' =>  'require',

            ];
            $msg = [
                'spe_img' =>  '图标不能为空',

            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            $res = Db::name('web_spe')->insert($data);
            if($res){
                $this->success('添加成功','webspe/index');
            }else{
                $this->error('添加失败');
            }
        }
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            //轮播图
            $file = request()->file('spe_img');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['spe_img']= $fonturl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }
          
          
          	
            //验证规则
            $rule = [
                'spe_img' =>  'require',

            ];
            $msg = [
                'spe_img' =>  '图标不能为空',

            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }
			

            Db::name('web_spe')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功', 'webspe/index');
        }
        $id=input('id');
        $banner=Db::name('web_spe')->where(['id'=>$id])->find();
        $this->assign('banner',$banner);
        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        Db::name('web_spe')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('禁用成功', 'webspe/index');
    }

}
