<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Zhifu extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();

        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);

        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
        //后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
  

    public function index(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            for($i=1;$i<=100;$i++){
                if(array_key_exists('value'.$i,$info)&&$info['value'.$i]){
                    $data['value']=$info['value'.$i];
                    Db::name('hide')->where(['id'=>$i])->update($data);
                }
            }

            $url=Db::name('hide')->where(['id'=>28])->value('value');

          
          	$file = request()->file('value42');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png,ico'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $value = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                    $data['value']=$url.$value;
                    Db::name('hide')->where(['id'=>42])->update($data);
                }else{
                    echo $file->getError();
                }
            }
          
          
            $file = request()->file('value43');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png,ico'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $value = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                    $data['value']=$url.$value;
                    Db::name('hide')->where(['id'=>43])->update($data);
                }else{
                    echo $file->getError();
                }
            }
            

            

            $this->success('修改成功','zhifu/index');
        }
        $list=Db::name('hide')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }
    


}
