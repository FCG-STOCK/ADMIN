<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class User extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index(){
        $list = Db::name('user')
            ->where(['is_del'=>1])
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            //查看管理员权限，展示指定左侧导航,可访问权限字段中展示
            $arr=explode(',',substr($v['auth'],0,-1));
            $str='';
            //可管理栏目名字
            foreach($arr as $kk=>$vv){
                foreach(config('navigation') as $kkk=>$vvv){
                    if($vv==$vvv){$str.=$kkk.',';}
                }
            }
            $list[$k]['auth']=$str;
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add() {
    	//LRSS 2019-2-17 ADD 权限判断
    	//查询当前用户的权限数组
    	$user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
    	if(empty($user)){
    		$this->error('无操作权限');
    	}else{
    		$auth_array = explode(',', trim($user['auth'],',')); //获得权限数组
    		if(!in_array('10', $auth_array)){
    			$this->error('无操作权限');
    		}
    	}
    	
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //查询账号名是否重复
            $user=Db::name('user')->where(['account'=>$info['account']])->find();
            if($user){$this->error('账号名已重复');}

            //管理员头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name2);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            $data['account']=$info['account'];
            $data['pwd']=$info['pwd'];
            $data['is_del']=1;
            $data['username']=$info['username'];
            //创建时添加权限
            $data['auth']='';
            if(array_key_exists('auth',$info)&&$info['auth']){
                foreach($info['auth'] as $k=>$v){
                    $data['auth'].=$v.',';
                }
            }
            $data['create_time']=time();

            //验证规则
            $rule = [
                'account' =>  'require',
                'pwd' =>  'require',
                'username' =>  'require',
                'headurl' =>  'require',
            ];
            $msg = [
                'account' =>  '账号不能为空',
                'pwd' =>  '密码不能为空',
                'username' =>  '名字不能为空',
                'headurl' =>  '头像不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //密码加密
            $data['pwd']=md5($info['pwd']);

            $res = Db::name('user')->insert($data);
            if($res){
                $this->success('添加成功','user/index');
            }else{
                $this->error('添加失败');
            }
        }
        //左端导航列表
        $this->assign('navigation',config('navigation'));
        return $this->fetch();
    }
    public function update() {
    	//LRSS 2019-2-17 ADD 权限判断
    	//查询当前用户的权限数组
    	$user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
    	if(empty($user)){
    		$this->error('无操作权限');
    	}else{
    		$auth_array = explode(',', trim($user['auth'],',')); //获得权限数组
    		if(!in_array('10', $auth_array)){
    			$this->error('无操作权限');
    		}
    	}
    	
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['account']=$info['account'];
            $data['username']=$info['username'];
            
            //LRSS 2019-2-17 EDIT 不修改user表中id为1的用户的权限
            if($info['id'] != 1){
	            //修改时修改权限
	            $data['auth']='';
	            if(array_key_exists('auth',$info)&&$info['auth']){
	                foreach($info['auth'] as $k=>$v){
	                    $data['auth'].=$v.',';
	                }
	            }
            }

            //验证规则
            $rule = [
                'account' =>  'require',
                'username' =>  'require',
            ];
            $msg = [
                'account' =>  '账号不能为空',
                'username' =>  '名字不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //管理员头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name2);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            Db::name('user')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功', 'user/index');
        }
        $id=input('id');
        $user=Db::name('user')->where(['id'=>$id])->find();
        $this->assign('user',$user);

        //当前人员权限数组
        $auth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$auth);

        //左端导航列表
        $this->assign('navigation',config('navigation'));
        return $this->fetch();
    }
    public function del(){
    	//LRSS 2019-2-17 ADD 权限判断
    	//查询当前用户的权限数组
    	$user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
    	if(empty($user)){
    		$this->error('无操作权限');
    	}else{
    		$auth_array = explode(',', trim($user['auth'],',')); //获得权限数组
    		if(!in_array('10', $auth_array)){
    			$this->error('无操作权限');
    		}
    	}
    	
        $id=input('id');
        Db::name('user')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('删除成功', 'user/index');
    }

}
