<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Newsdetail extends Controller {

    //新闻详情页
    public function newsdetail() {
        //浏览次数加1
        Db::name('news')->where(['id' => input('id')])->setInc('times');
        $info = Db::name('news')->where(['id' => input('id')])->find();
        $info['create_time'] = date('Y-m-d H:i:s', $info['create_time']);
        $this->assign('news_info', $info);
        return $this->fetch();
    }

}