<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Marketer extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['id']=substr(cookie('marketer_user'),0,3);
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
        }else{
            //分销商
            $where['id']=cookie('marketer');
        }

        $list = Db::name('marketer')->where($where)->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            //工作人员总数
            $list[$k]['usercount'] = Db::name('marketer_user')->where(['marketer_id'=>$v['id'],'is_del'=>1])->count();
            //会员总数
            $list[$k]['admincount'] = Db::name('admin')->where(['marketer_id'=>$v['id'],'is_del'=>1])->count();
        }
        $this->assign('list',$list);
        //是否为分销商
        $this->assign('marketer',cookie('marketer'));

        return $this->fetch();
    }
    public function add() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //查询账号名是否重复
            $user=Db::name('marketer')->where(['account'=>$info['account']])->find();
            if($user){$this->error('账号名已重复');}

            $data['account']=$info['account'];
            $data['pwd']=$info['pwd'];
            $data['username']=$info['username'];
            $data['is_real_disk']=$info['is_real_disk'];
            if($info['is_real_disk']==1){
                $data['shipan_account']=$info['shipan_account'];
            }else{
                $data['shipan_account']='';
            }
            $data['auth']='2,3,4,5,8,9,';
            $data['is_del']=1;
            $data['create_time']=time();

            //验证规则
            if(!$data['account']){
                $this->error('账号不能为空');
            }
            $rule = [
                'pwd' =>  'require',
                'username' =>  'require',
            ];
            $msg = [
                'pwd' =>  '登陆密码不能为空',
                'username' =>  '名字不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //用户头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
//                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,gif'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            //密码加密
            $data['pwd']=md5($info['pwd']);

            //分销商id从801开始
            if(!Db::name('marketer')->limit(1)->find()){$data['id']=801;}

            $res = Db::name('marketer')->insert($data);
            if($res){
                $this->success('添加成功','marketer/index');
            }else{
                $this->error('添加失败');
            }
        }
        $shipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
        $this->assign('shipan_account',$shipan_account);
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['account']=$info['account'];
            //有密码则修改数据库
            if($info['pwd']){
                $data['pwd']=md5($info['pwd']);
            }
            $data['username']=$info['username'];

            //如果要修改实盘信息的话
            if($info['updshipan']==1){
                $data['is_real_disk']=$info['is_real_disk'];
                //如果进入实盘的话，选择账户才有效
                if($info['is_real_disk']==1){
                    $data['shipan_account']=$info['shipan_account'];
                }else{
                    $data['shipan_account']='';
                }
            }

            //LRSS 2019-2-20 ADD 获取分销商手续费和利息信息
            $parent_info=Db::name('marketer')->where(['id'=>$info['id']])->find();
            if(empty($parent_info)){
                $this->error('分销商信息不存在');
            }

            //验证规则
            if(!$data['account']){
                $this->error('账号不能为空');
            }
            $rule = [
                'username' =>  'require',
            ];
            $msg = [
                'username' =>  '名字不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //用户头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
//                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,gif'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }
            //修改分销商信息
            Db::name('marketer')->where(['id'=>$info['id']])->update($data);

            //如果要修改实盘信息的话
            //修改此分销商所有的会员的实盘信息
            if($info['updshipan']==1){
                Db::name('admin')
                    ->where(['marketer_id'=>$info['id']])
                    ->update(['is_real_disk'=>$data['is_real_disk'],'shipan_account'=>$data['shipan_account']]);
            }

            $this->success('修改成功', 'marketer/index');
        }
        $id=input('id');
        $user=Db::name('marketer')->where(['id'=>$id])->find();
        $this->assign('user',$user);

        $shipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
        $this->assign('shipan_account',$shipan_account);

        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        $res=Db::name('admin')->where(['marketer_id'=>$id])->select();
        if($res){
            $this->error('禁用失败,本分销商下还拥有会员');
        }
        Db::name('marketer')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('禁用成功', 'marketer/index');
    }
    //会员信息
    public function admin(){
        if(input('marketer_user')){
            $where['marketer_user']=input('marketer_user');
        }
        $where['marketer_id']=input('marketer_id');
        $where['is_del']=1;
        $list = Db::name('admin')->where($where)->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d',$v['lastlogin_time']);
            if($v['is_del']==1){
                $list[$k]['is_del']='正常';
            }else{
                $list[$k]['is_del']='禁用';
            }
        }
        $this->assign('list',$list);
        return $this->fetch();
    }
    //分销工作人员信息
    public function staffindex(){
        $where['marketer_id']=input('id');
        $where['is_del']=1;
        $list = Db::name('marketer_user')
            ->where($where)
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            //会员个数
            $list[$k]['admincount'] = Db::name('admin')->where(['marketer_user'=>$v['id'],'is_del'=>1])->count();
        }

        $this->assign('list',$list);
        //分销商id
        $this->assign('marketer_id',input('id'));
        $this->assign('del',input('del'));
        return $this->fetch();
    }
    //添加工作人员
    public function staffadd() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //查询账号名是否重复
            $user=Db::name('marketer_user')->where(['account'=>$info['account']])->find();
            if($user){$this->error('账号名已重复');}

            $data['marketer_id']=$info['marketer_id'];
            $data['account']=$info['account'];
            $data['pwd']=$info['pwd'];
            $data['username']=$info['username'];
            $data['auth']='2,3,4,5,9,';
            $data['is_del']=1;
            $data['create_time']=time();

            //LRSS 2019-2-10 ADD 增加手续费和利息
            $data['fee']=$info['fee'];
            $data['interest']=$info['interest'];

            //LRSS 2019-2-20 ADD 获取上级分销商的手续费和利息信息
            $parent_info=Db::name('marketer')->where(['id'=>$info['marketer_id']])->find();
            if(empty($parent_info)){
                $this->error('分销商信息不存在');
            }

            //验证规则
            if(!$data['account']){
                $this->error('账号不能为空');
            }
            $rule = [
                'pwd' =>  'require',
                'username' =>  'require',

                //LRSS 2019-2-10 ADD 增加手续费和利息
                'fee'   => 'require|integer|between:0,'.$parent_info['fee'],
                'interest'   => 'require|integer|between:0,100'.$parent_info['interest'],
            ];
            $msg = [
                'pwd' =>  '登陆密码不能为空',
                'username' =>  '名字不能为空',

                //LRSS 2019-2-10 ADD 增加手续费和利息
                'fee.require' =>  '手续费不能为空',
                'fee.integer' =>  '手续费必须是整数',
                'fee.between' =>  '手续费必须在0~'.$parent_info['fee'].'之间',
                'interest.require' =>  '利息不能为空',
                'interest.integer' =>  '利息必须是整数',
                'interest.between' =>  '利息必须在0~'.$parent_info['interest'].'之间',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //用户头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
//                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,gif'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            //密码加密
            $data['pwd']=md5($info['pwd']);

            //分销商工作人员id从xxx001开始
            $market_user=Db::name('marketer_user')->where(['marketer_id'=>$info['marketer_id']])->order('create_time desc')->limit(1)->find();
            if($market_user){
                $data['id']=$market_user['id']+1;
            }else{
                $market=Db::name('marketer')->where(['id'=>$info['marketer_id']])->limit(1)->find();
                $data['id']=$market['id'].'001';
            }

            $res = Db::name('marketer_user')->insert($data);
            if($res){
                $this->success('添加成功',url('marketer/staffindex',['id'=>$info['marketer_id'],'del'=>$info['del']]));
            }else{
                $this->error('添加失败');
            }
        }
        $this->assign('marketer_id',input('marketer_id'));
        $this->assign('del',input('del'));
        return $this->fetch();
    }
    //修改工作人员
    public function staffupdate() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['account']=$info['account'];
            //有密码则修改数据库
            if($info['pwd']){
                $data['pwd']=md5($info['pwd']);
            }
            $data['username']=$info['username'];

            //LRSS 2019-2-10 ADD 增加手续费和利息
            $data['fee']=$info['fee'];
            $data['interest']=$info['interest'];

            //LRSS 2019-2-20 ADD 获取分销商的手续费和利息信息
            $parent_info=Db::name('marketer')->where(['id'=>$info['marketer_id']])->find();
            if(empty($parent_info)){
                $this->error('分销商信息不存在');
            }

            //验证规则
            if(!$data['account']){
                $this->error('账号不能为空');
            }

            $rule = [
                'username' =>  'require',

                //LRSS 2019-2-10 ADD 增加手续费和利息
                'fee'   => 'require|integer|between:0,'.$parent_info['fee'],
                'interest'   => 'require|integer|between:0,'.$parent_info['interest'],
            ];
            $msg = [
                'username' =>  '名字不能为空',

                //LRSS 2019-2-10 ADD 增加手续费和利息
                'fee.require' =>  '手续费不能为空',
                'fee.integer' =>  '手续费必须是整数',
                'fee.between' =>  '手续费必须在0~'.$parent_info['fee'].'之间',
                'interest.require' =>  '利息不能为空',
                'interest.integer' =>  '利息必须是整数',
                'interest.between' =>  '利息必须在0~'.$parent_info['interest'].'之间',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            //用户头像
            $file = request()->file('headurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
//                $name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,gif'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['headurl']= str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            Db::name('marketer_user')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功', url('marketer/staffindex',['id'=>$info['marketer_id'],'del'=>$info['del']]));
        }
        $id=input('id');
        $user=Db::name('marketer_user')->where(['id'=>$id])->find();
        $this->assign('user',$user);
        $this->assign('del',input('del'));
        $this->assign('marketer_id',input('marketer_id'));
        return $this->fetch();
    }
    //禁用工作人员
    public function staffdel(){
        $info = Request::instance()->param();

        $res=Db::name('admin')->where(['marketer_id'=>$info['marketer_id'],'marketer_user'=>$info['id'],'is_del'=>1])->select();
        if($res){
            $this->error('禁用失败,本工作人员下还拥有会员');
        }
        Db::name('marketer_user')->where(['id'=>$info['id']])->update(['is_del'=>2]);
        $this->success('禁用成功', url('marketer/staffindex',['id'=>$info['marketer_id'],'del'=>$info['del']]));
    }




}
