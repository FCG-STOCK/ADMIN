<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Forbidden extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();

        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);

        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index(){
        $list = Db::name('forbidden_list')
            ->where(['is_del'=>1])
            ->select();
        foreach($list as $k=>$v){
            
            //轮播图状态
            if($v['is_del']==1){
                $list[$k]['is_del']='未禁用';
            }else{
                $list[$k]['is_del']='已禁用';
            }
            
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            
            $data['stock_code']=$info['stock_code'];
          	$link_forbidden_list = Db::name('forbidden_list')->where('stock_code',$data['stock_code'])->find();
          	if($link_forbidden_list){
            	$this->error('请勿重复添加');
            }
            $json=getOneStock2($info['stock_code']); 
          	if($json==100){
            	$this->error('该股票不存在');
            }
            $data['stock_name']=json_decode($json,true)['list']['result'][0]['data']['name'];

            $data['is_del']=1;

            //验证规则
            $rule = [
                'stock_code' =>  'require',
            ];
            $msg = [
                'stock_code' =>  '股票不能为空',                
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            $res = Db::name('forbidden_list')->insert($data);
            if($res){
                $this->success('添加成功','forbidden/index');
            }else{
                $this->error('添加失败');
            }
        }
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            
            $data['stock_code']=$info['stock_code'];
          
          	$link_forbidden_list = Db::name('forbidden_list')->where('stock_code',$data['stock_code'])->find();
          	if($link_forbidden_list){
            	$this->error('请勿重复添加');
            }
          
            $json=getOneStock2($info['stock_code']);  

          	if($json==100){
            	$this->error('该股票不存在');
            }
            $data['stock_name']=json_decode($json,true)['list']['result'][0]['data']['name'];

            $data['is_del']=1;

            //验证规则
            $rule = [
                'stock_code' =>  'require',
            ];
            $msg = [
                'stock_code' =>  '股票不能为空',                
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            Db::name('forbidden_list')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功', 'forbidden/index');
        }
        $id=input('id');
        $banner=Db::name('forbidden_list')->where(['id'=>$id])->find();
        $this->assign('banner',$banner);
        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        Db::name('forbidden_list')->where(['id'=>$id])->delete();
        $this->success('禁用成功', 'forbidden/index');
    }

}
