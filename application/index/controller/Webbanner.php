<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Webbanner extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();

        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);

        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
        //后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index(){
        $list = Db::name('web_banner')
            ->where(['is_del'=>1])
            ->order('sort desc')
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            //轮播图状态
            if($v['is_del']==1){
                $list[$k]['is_del']='未禁用';
            }else{
                $list[$k]['is_del']='已禁用';
            }
            //1首页  2新闻中心  3关于我们   4新手引导'
            if($v['type']==1){
                $list[$k]['type']='首页';
            }elseif($v['type']==2){
                $list[$k]['type']='新闻中心';
            }elseif($v['type']==3){
                $list[$k]['type']='关于我们';
            }else{
            	$list[$k]['type']='新手引导';
            }
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            //轮播图
            $file = request()->file('web_bannerurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['web_bannerurl']= $fonturl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            $data['sort']=$info['sort'];
            $data['is_del']=1;
            $data['create_time']=time();
            $data['type']=$info['type'];
          	$data['banner_title']=$info['banner_title'];
          	$data['banner_wenan']=$info['banner_wenan'];
          	$data['banner_link']=$info['banner_link'];
            //验证规则
            $rule = [
                'web_bannerurl' =>  'require',
                'sort' =>  'number',
            ];
            $msg = [
                'web_bannerurl' =>  'banner图不能为空',
                'sort' =>  '排序只能为数字',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            $res = Db::name('web_banner')->insert($data);
            if($res){
                $this->success('添加成功','webbanner/index');
            }else{
                $this->error('添加失败');
            }
        }
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');

            //轮播图
            $file = request()->file('web_bannerurl');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $data['web_bannerurl']= $fonturl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                }else{
                    echo $file->getError();
                }
            }

            $data['sort']=$info['sort'];
            $data['type']=$info['type'];
            $data['create_time']=time();
          	$data['banner_title']=$info['banner_title'];
          	$data['banner_wenan']=$info['banner_wenan'];
          	$data['banner_link']=$info['banner_link'];
            //验证规则
            $rule = [
                'sort' =>  'number',
            ];
            $msg = [
                'sort' =>  '排序只能为数字',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            Db::name('web_banner')->where(['id'=>$info['id']])->update($data);
            $this->success('修改成功', 'webbanner/index');
        }
        $id=input('id');
        $banner=Db::name('web_banner')->where(['id'=>$id])->find();
        $this->assign('banner',$banner);
        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        Db::name('web_banner')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('禁用成功', 'webbanner/index');
    }

}
