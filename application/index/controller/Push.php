<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Push extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index() {
        $list = Db::name('push')
            ->where(['account'=>0])
            ->order('create_time desc')
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            $list[$k]['content']=mb_substr($v['content'],0,50);
            //推送是否成功
            if($v['status']==1){
                $list[$k]['status']='成功';
            }else{
                $list[$k]['status']='失败';
            }
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    //推送
    protected $app_key = 'd20e4e0a17b889f14e96114f';
    protected $master_secret = '9dcef1129b29f733cb40e67f';
    protected function push($content='') {
        $client = new \JPush\Client($this->app_key, $this->master_secret);
        $result=$client->push()
            ->setPlatform('all')
            ->addAllAudience()
            ->setNotificationAlert($content)
            ->send();
        return $result;
    }
    public function add(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['content']=$info['content'];
            $data['push_type']=1;
            $data['create_time']=time();

            //验证规则
            $rule = [
                'content' =>  'require',
            ];
            $msg = [
                'content' =>  '内容不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)) {
                $this->error($validate->getError());
            }

            //进行全员推送
            $result=$this->push($data['content']);
            if($result['http_code']==200){
                $data['status']=1;
            }else{
                $data['status']=2;
            }

            //推送结果需要写入库中
            $res = Db::name('push')->insert($data);
            if($res){
                $this->success('推送成功','push/index');
            }else{
                $this->error('推送失败');
            }
        }
        return $this->fetch();
    }

}
