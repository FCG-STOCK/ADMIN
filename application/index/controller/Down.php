<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Down extends Controller
{
    //下载
    public function down(){
        $h5logo=Db::name('hide')->where(['id'=>31])->value('value');
        $this->assign('h5logo',$h5logo);
        $ico=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('ico',$ico);
        $title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('title',$title);
        return $this->fetch();
    }
    //h5注册页
    public function registerh5(){
        $register = input('register');
        $this->assign('register',$register);
        return $this->fetch();
    }

    //发送短信
    public function sendMessage2() {
        $info = Request::instance()->param();
        $mobile = $info['mobile'];

        $user=Db::name('admin')->where(['account'=>$mobile])->find();
        if($user){return json('', 1103, '用户名已存在');}

        $rand = rand(1000, 9999);
        session('yzm',$rand);
        session('mobile',$mobile);
        $json_data=sendMessage($rand,$mobile);
        $array = json_decode($json_data, true);
        if ($array['code'] == 0) {
            return json($list['rand'] = strval(md5($rand)), 1, '发送成功');
        } else {
            return json('', 0, '发送失败');
        }
    }
    //注册
    public function register2() {
        $info = Request::instance()->param();

        $user=Db::name('admin')->where(['account'=>$info['Account']])->find();
        if($user){return json('', 1103, '用户名已存在');}

        // $data['regid'] = $info['regid'];
        $data['account'] = $info['Account'];
        $data['nick_name'] = $info['Account'];
        $data['pwd'] = $info['pwd'];
        $data['is_del'] = 1;
        $data['inviter'] = $info['inviter'];//邀请码

        $yzm = $info['yzm'];

        $yzmse = session('yzm');

        $tel = session('mobile');

        if (!$data['account']) {
            return json('', 0, '用户名不能为空');
        }
        if (!$yzm) {
            return json('', 1100, '验证码不能为空');
        }

        if($yzmse != $yzm){
            return json('',1101,"验证码不正确或已过期");
        }

        if($data['account'] != $tel){
            return json('',1102,"输入手机号和发送验证码手机不同");
        }

        $data['lastlogin_time'] = time();
        $data['create_time'] = time();


        //是否有邀请人,可填手机号或分销商工作人员编号,根据邀请人获得不同id
        if(array_key_exists('inviter',$info)&&$info['inviter']){
            //判断工作人员表中是否有此分销商工作人员编号
            $marketuser=Db::name('marketer_user')->where(['id'=>$info['inviter'],'is_del'=>1])->find();
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['inviter'],'is_del'=>1])->find();
            //判断邀请人
            if($marketuser){
                //最近一次受此工作人员推广的用户  例8010012
                $list=Db::name('admin')->where(['marketer_user'=>$info['inviter'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']=substr($id,0,6).(substr($id,6)+1);
                }else{
                    $data['id']=$info['inviter'].'1';
                }
                //添加分销商id和工作人员id
                $data['marketer_id']=substr($info['inviter'],0,3);
                $data['marketer_user']=$info['inviter'];
                //添加工作人员id到邀请人
                $data['inviter']=$info['inviter'];
            }elseif($inviter){
                //最近一次没有分销商的普通用户  例0000002
                $list=Db::name('admin')->where(['marketer_user'=>$info['inviter'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']='000000'.($id+1);
                }else{
                    $data['id']='0000001';
                }
                //添加手机号到邀请人
                $data['inviter']=$info['inviter'];
            }else{
                return json('', 0, '邀请人不存在');
            }
        }else{
            //最近一次没有分销商的普通用户  例0000002
            $list=Db::name('admin')->field('id')->where(['marketer_id'=>0,'is_del'=>1])->select();
            array_multisort(array_column($list,'id'),SORT_DESC,$list);
            if($list){
                $id=$list[0]['id'];
                $data['id']='000000'.($id+1);
            }else{
                $data['id']='0000001';
            }
        }


        $rule = [
            'account' => '^1[345789]\d{9}$',
            'pwd'     => 'require',
            'inviter' => 'number',
        ];
        $msg = [
            'account' => '用户名必须是手机号',
            'pwd'     => '密码不能为空',
            'inviter' => '邀请码必须是数字',
        ];
        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)) {
            return json('', 0, $validate->getError());
        }

        $data['pwd'] = md5($data['pwd']);
        //注册时，每个人默认有100000元炒股大赛的模拟资金
        $data['analog_money']=0;
        //注册时，用户的默认头像
      	
      	$apiurl = Db::name('hide')->where('id',25)->value('value');
      
        $data['headurl']=$apiurl.'/public/all/image/geren/grt.png';
        $res = Db::name('admin')->insert($data);
        if (!$res) {
            return json('', 0, '注册失败');
        }


        //红包有效期
        $redday=Db::name('hide')->where(['id'=>11])->value('value');
        //发送红包
        if(array_key_exists('inviter',$info)&&$info['inviter']){
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['inviter'],'is_del'=>1])->find();
            if($inviter){
                //给这个邀请人发放1个红包
                $para=[
                    'account'=>$info['inviter'],
                    'packet_name'=>'邀请奖励红包',
                    'packet_money'=>10,
                    'is_use'=>2,
                    'create_time'=>time(),
                    'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
                ];
                Db::name('packet')->insert($para);
            }
        }

        //新注册用户获得5个红包
        $para=[
            'account'=>$info['Account'],
            'packet_name'=>'新人红包',
            'packet_money'=>10,
            'is_use'=>2,
            'create_time'=>time(),
            'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
        ];
        for($i=0;$i<5;$i++){
            Db::name('packet')->insert($para);
        }

        //添加个人设置
        Db::name('settings')->insert(['account'=>$info['Account']]);

        session('username',$data['account']);
        cookie('username',$data['account']);
        cookie('nick_name',$data['nick_name']);
        return json('', 1, '注册成功');
    }


}
