<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class News extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index() {
        $info=Request::instance()->param();

        if(array_key_exists('cid',$info)&&$info['cid']){
            $where['cid']=$info['cid'];
        }
        $where['is_del']=1;

        $list = Db::name('news')
            ->order('sort desc,create_time desc')
            ->where($where)
            ->select();
        //查询新闻分类
        $cate=Db::name('news_category')->select();
        $this->assign('cate',$cate);

        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            if($v['is_recommend']==1){
                $list[$k]['is_recommend']='推荐';
            }else{
                $list[$k]['is_recommend']='未推荐';
            }
            //获取分类名称
            foreach($cate as $kk=>$vv){
                if($v['cid']==$vv['cid']){$list[$k]['cid']=$vv['cname'];}
            }
        }
        $this->assign('list',$list);
        //获取当前分类id
        $this->assign('cid',input('cid'));
        return $this->fetch();
    }

    public function add(){
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['cid']=$info['cid'];
            $data['title']=$info['title'];
          	$data['synopsis']=$info['synopsis'];
            $data['is_recommend']=$info['is_recommend'];
            //因为百度编辑器，判断是否提交内容
            if(array_key_exists('content',$info)&&$info['content']){
                $data['content']=$info['content'];
            }
            $data['times']=rand(500,2000);
            $data['is_del']=1;
            $data['create_time']=time();
            //因为百度编辑器，判断是否提交内容
            if(array_key_exists('content',$info)&&$info['content']){
                $data['content']=$info['content'];
                //文字
                $str=$data['content'];
                preg_match_all('/[\x{4e00}-\x{9fff}]+/u', $str, $matches);
                $str = join('', $matches[0]);
                $str = str_replace('微软雅黑','',$str);
                $str = str_replace('冬青黑体简体中文','',$str);
                $str = str_replace('华文细黑','',$str);
                $str = str_replace('宋体','',$str);
                $str = str_replace('黑体','',$str);
                $data['words'] = $str;
            }

            if(!$data['cid']){
                $this->error('文章类型不能为空');
            }
            //验证规则
            $rule = [
                'title' =>  'require',
                'is_recommend' =>  'require',
                'content' =>  'require',
            ];
            $msg = [
                'title' =>  '标题不能为空',
                'is_recommend' =>  '是否推荐不能为空',
                'content' =>  '内容不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)) {
                $this->error($validate->getError());
            }

          	//后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');
          
            $arr=explode('/public',htmlspecialchars($data['content']));
            if(strpos($data['content'],$fonturl.'/public') || strpos($data['content'],$fonturl.'/public')===0){
            }else{
                $data['content'] = str_replace('/public',$fonturl.'/public',$data['content']); 
            }
            //去掉第一个没用数据
            unset($arr[0]);
            if(!$arr){
                $this->error('请至少添加一张图片');
            }
            $data['picurl']='';
            foreach($arr as $k=>$v){
                if(strpos(substr($v,0,38),'.jpe')!== false){
                    $data['picurl'].=$fonturl.'/public'.substr($v,0,38).'g,';
                }else{
                    $data['picurl'].=$fonturl.'/public'.substr($v,0,38).',';
                }
            }


            $res = Db::name('news')->insert($data);
            if($res){
                //返回到当前的新闻分类页面
                $this->success('添加成功',url('news/index',['cid'=>$data['cid']]));
            }else{
                $this->error('添加失败');
            }
        }
        //获取新闻分类
        $cate=Db::name('news_category')->select();
        $this->assign('cate',$cate);
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['cid']=$info['cid'];
            $data['title']=$info['title'];
          	$data['synopsis']=$info['synopsis'];
            $data['is_recommend']=$info['is_recommend'];

            //因为百度编辑器，判断是否提交内容
            if(array_key_exists('content',$info)&&$info['content']){
                $data['content']=$info['content'];
                //文字
                $str=$data['content'];
                preg_match_all('/[\x{4e00}-\x{9fff}]+/u', $str, $matches);
                $str = join('', $matches[0]);
                $str = str_replace('微软雅黑','',$str);
                $str = str_replace('冬青黑体简体中文','',$str);
                $str = str_replace('华文细黑','',$str);
                $str = str_replace('宋体','',$str);
                $str = str_replace('黑体','',$str);
                $data['words'] = $str;
            }

            $data['sort']=$info['sort'];

            //验证规则
            $rule = [
                'cid' =>  'require',
                'title' =>  'require',
                'is_recommend' =>  'require',
                'content' =>  'require',
            ];
            $msg = [
                'cid' =>  '文章类型不能为空',
                'title' =>  '标题不能为空',
                'is_recommend' =>  '是否推荐不能为空',
                'content' =>  '内容不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }


            
          	//后台地址
            $fonturl=Db::name('hide')->where(['id'=>28])->value('value');
          
            $arr=explode('/public',htmlspecialchars($data['content']));
            if(strpos($data['content'],$fonturl.'/public') || strpos($data['content'],$fonturl.'/public')===0){
            }else{
                $data['content'] = str_replace('/public',$fonturl.'/public',$data['content']); 
            }
            //去掉第一个没用数据
            unset($arr[0]);
            if(!$arr){
                $this->error('请至少添加一张图片');
            }
            $data['picurl']='';
            foreach($arr as $k=>$v){
                if(strpos(substr($v,0,38),'.jpe')!== false){
                    $data['picurl'].=$fonturl.'/public'.substr($v,0,38).'g,';
                }else{
                    $data['picurl'].=$fonturl.'/public'.substr($v,0,38).',';
                }
            }


            Db::name('news')->where(['id'=>$info['id']])->update($data);
            //返回到当前的新闻分类页面
            $this->success('修改成功',url('news/index',['cid'=>$info['oldcid']]));
        }
        $id=input('id');
        $news=Db::name('news')->where(['id'=>$id])->find();
        $this->assign('news',$news);

        //获取新闻分类
        $cate=Db::name('news_category')->select();
        $this->assign('cate',$cate);
        return $this->fetch();
    }
    public function del(){
        $id=input('id');
        $cid=input('cid');
        Db::name('news')->where(['id'=>$id])->update(['is_del'=>2]);
        $this->success('删除成功',url('news/index',['cid'=>$cid]));
    }


}
