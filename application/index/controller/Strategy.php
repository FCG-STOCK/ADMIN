<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
class Strategy extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }

    public function index() {
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }

        $info = Request::instance()->param();
        //搜索中的策略种类查询
        if(array_key_exists('strategy_type',$info)&&$info['strategy_type']){
            $where['str.strategy_type'] = $info['strategy_type'];
            $this->assign('strategy_typed',$info['strategy_type']);
        }
        //搜索中的策略状态查询
        if(array_key_exists('status',$info)&&$info['status']){
            $where['str.status'] = $info['status'];
            $this->assign('statusd',$info['status']);
        }
        //搜索中的是否实盘查询
        if(array_key_exists('is_real_disk',$info)&&$info['is_real_disk']){
            $where['str.is_real_disk'] = $info['is_real_disk'];
            $this->assign('is_real_disked',$info['is_real_disk']);
        }
        //统计数据跳转过来的账号查询
        if(array_key_exists('account',$info)&&$info['account']){
            $where['ad.account'] = $info['account'];
            $this->assign('account',$info['account']);
        }
        //统计数据跳转过来的分销商查询
        if(array_key_exists('mark',$info)&&$info['mark']){
            $where['ad.marketer_id'] = $info['mark'];
            $this->assign('mark',$info['mark']);
        }
        //统计数据跳转过来的时间查询（天周月）1为当天 2为当周 3为当月
        if(array_key_exists('date',$info)&&$info['date']==1){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==2){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime("this week Monday")];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==3){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==4){
            $strategywhere=[];
            $this->assign('date',$info['date']);
        }else{
            $strategywhere=[];
        }


        $list = Db::name('strategy')
            ->alias('str')
            ->field('ad.account,ad.true_name,str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($where)
            ->where($strategywhere)
            ->where(['str.is_cancel_order'=>1])
            ->select();

        if($list){
            //获取当前单价
            $strategyarr=[];
            $cishu=ceil(count($list)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($list as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
              	//20190507修改
              	$shipanstock=new Stock();
                if(date('Hi')>=930){
                    $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                    $arr=explode(';',$result);
                    unset($arr[count($arr)-1]);
                    foreach($arr as $kk=>$vv){
                        $strategyarr[$k][$kk]['nowprice']=substr(explode(',',$vv)[3],0,-1);
                    }
                
                }else{
                
                    $res=$shipanstock->Hq_real_list($str);
                    $res = json_decode($res,true);
                    foreach($res as $kk=>$vv){
                        $strategyarr[$k][$kk]['nowprice']=$res[$kk]['nowPri'];
                    }
                }
                //$result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                //$arr=explode(';',$result);
                //unset($arr[count($arr)-1]);
                //foreach($arr as $kk=>$vv){
                    //$strategyarr[$k][$kk]['nowprice']=substr(explode(',',$vv)[3],0,-1);
                //}

            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $list[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }

            foreach($list as $k=>$v){
                //策略种类  T+1 T+5
                foreach(config('strategy_type') as $kk=>$vv){
                    if($v['strategy_type']==$vv){$list[$k]['strategy_type']=$kk;}
                }
                //已平仓的按照平仓价格计算
                if($v['status']==3||$v['status']==4){
                    //策略盈亏(收益)
                    $list[$k]['income'] = round($v['sell_price']*$v['stock_number']-$v['market_value'],2);
                    //涨/跌幅比率
                    $list[$k]['rate'] = round((($v['sell_price']-$v['buy_price'])/$v['buy_price']*100),2).'%';
                    //持仓时间
                    $list[$k]['defer_day'] = round((strtotime(date('Ymd',$v['sell_time']))-strtotime(date('Ymd',$v['buy_time'])))/3600/24)-$v['rest_day'];
                }else{
                    //策略盈亏(收益)
                    $list[$k]['income']=round($v['nowprice']*$v['stock_number']-$v['market_value'],2);
                    //涨/跌幅比率
                    $list[$k]['rate'] = round((($v['nowprice']-$v['buy_price'])/$v['buy_price']*100),2).'%';
                    //持仓时间
                    $list[$k]['defer_day'] = round((strtotime(date('Ymd'))-strtotime(date('Ymd',$v['buy_time'])))/3600/24)-$v['rest_day'];
                }
				if($v['status']==2||$v['status']==4){
                    $list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
                }
                if($v['status']==4){
                    $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
                }
                //策略状态
                if($v['status']==1){
                    $list[$k]['status']='持仓申报中';
                }elseif($v['status']==2){
                    $list[$k]['status']='持仓中';
                }elseif($v['status']==3){
                    $list[$k]['status']='平仓申报中';
                }elseif($v['status']==4){
                    $list[$k]['status']='已平仓';
                }
            }
            //搜索中的策略盈亏查询
            if(array_key_exists('income',$info)&&$info['income']){
                if($info['income']==1){
                    foreach ($list as $k => $v) {
                        if($v['income']<0){unset($list[$k]);}
                    }
                }else{
                    foreach ($list as $k => $v) {
                        if($v['income']>=0){unset($list[$k]);}
                    }
                }
                $this->assign('incomed',$info['income']);
            }
        }else{
            $list=[];
        }

        $this->assign('list',$list);
        $this->assign('marketer_user',cookie('marketer_user'));
      	
      	//查询所有的实盘持股的市值的总和
        $market_value_all=0;
        foreach($list as $k=>$v){
            if($v['status']=='持仓中'&&$v['is_real_disk']==1&&$v['is_cancel_order']==1){
                $market_value_all+=$v['market_value'];
            }
        }
        $this->assign('market_value_all',$market_value_all);
      	
        return $this->fetch();
    }

    //LRSS 2019-2-25 ADD 递延策略列表
    public function deferStrategy(){
        if(cookie('marketer_user')){
            //分销商工作人员deferstrategy
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')){
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }
        $info = Request::instance()->param();

        //搜索中的策略状态查询
        if(isset($info['status']) && $info['status']){
            $where['str.status'] = $info['status'];
            $this->assign('statusd',$info['status']);
        }
        //搜索中的是否实盘查询
        if(isset($info['is_real_disk']) && $info['is_real_disk']){
            $where['str.is_real_disk'] = $info['is_real_disk'];
            $this->assign('is_real_disked',$info['is_real_disk']);
        }

        //账号
        if(isset($info['account'])){
            $where['ad.account'] = $info['account'];
            $this->assign('account',$info['account']);
        }

        //分销商
        if(!cookie('marketer') && !cookie('marketer_user')){
            if(isset($info['mark'])){
                $where['ad.marketer_id'] = $info['mark'];
                $this->assign('mark',$info['mark']);
            }
        }

        //策略类型
        if(isset($info['strategy_type'])){
            $where['str.strategy_type'] = $info['strategy_type'];
            $this->assign('strategy_type',$info['strategy_type']);
        }

        //时间筛选
        if(isset($info['begin_time']) || isset($info['end_time'])){
            $begin_time = isset($info['begin_time']) ? strtotime($info['begin_time']) : 0;
            $end_time = isset($info['end_time']) ? strtotime($info['end_time']) : 0;

            if($end_time && $end_time < $begin_time){
                $temp_time = 0;
                $temp_time = $end_time;
                $end_time = $begin_time;
                $begin_time = $temp_time;
            }

            if($end_time){
                $end_time = $end_time + 86400;
            }
            if($begin_time && $end_time){
                $where['tra.create_time'][] = array('>=',$begin_time);
                $where['tra.create_time'][] = array('<',$end_time);
            }else{
                if($begin_time){
                    $where['tra.create_time'] = array('>=',$begin_time);
                }
                if($end_time){
                    $where['tra.create_time'] = array('<',$end_time);
                }
            }
            $this->assign('begin_time',$begin_time);
            $this->assign('end_time',$end_time);
        }else{
            if(isset($info['date'])){
                if($info['date'] == 1){
                    //今天
                    $begin_time = strtotime('today');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$begin_time + 86400);
                }
                if($info['date'] == 2){
                    //本周
                    $begin_time = strtotime('this week 00:00:00');
                    $end_time = strtotime('next week 00:00:00');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$end_time);
                }
                if($info['date'] == 3){
                    //本月
                    $begin_time = strtotime('first Day of this month 00:00:00');
                    $end_time = strtotime('first Day of next month 00:00:00');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$end_time);
                }
                if($info['date'] == 4){
                    //全部
                }
                $this->assign('date',$info['date']);
            }
        }

        //获取递延信息
        $defer_res = Db::name('trade')
            ->alias('tra')
            ->field('tra.trade_price,tra.create_time,ad.true_name,str.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->join('link_strategy str','str.strategy_num=tra.charge_num')
            ->where(['tra.trade_type'=>'扣除递延费'])
            ->where(['tra.trade_status'=>1])
            ->where(['tra.is_del'=>1])
            ->where(['ad.is_del'=>1])
            ->where($where)
            ->select();

        foreach($defer_res as $k=>&$v){
            //策略状态
            if($v['status']==1){
                $v['format_status']='持仓申报中';
            }elseif($v['status']==2){
                $v['format_status']='持仓中';
            }elseif($v['status']==3){
                $v['format_status']='平仓申报中';
            }elseif($v['status']==4){
                $v['format_status']='已平仓';
            }

            //买入时间
            if($v['status']==2||$v['status']==4){
                $v['format_buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
            }

            //卖出时间
            if($v['status']==4){
                $v['format_sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
            }else{
                $v['format_sell_time'] = '';
            }

            //策略类型
            if($v['strategy_type'] == 1){
                $v['format_strategy_type'] = 'T+1';
            }else{
                $v['format_strategy_type'] = 'T+5';
            }

            $v['format_create_time'] = date('Y-m-d H:i:s',$v['create_time']);
        }
        $this->assign('list',$defer_res);

        return $this->fetch();
    }

    //策略详情
    public function detail(){
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        //策略
        $strategy = Db::name('strategy')
            ->alias('str')
            ->field('ad.account,str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where(['str.id'=>input('id')])
            ->where(['str.is_cancel_order'=>1])
            ->find();
		
      	//20190507修改
        $shipanstock=new Stock();
        if(date('Hi')>=930){
          //股票当前的详细数据
          $json = getOneStock($strategy['stock_code']);
          $stock = json_decode($json, true);
          //股票当前价格
          $nowprice = $stock['result'][0]['data']['nowPri'];
          $strategy['nowprice']=$nowprice;

        }else{
          $res=$shipanstock->Hq_real_list($strategy['stock_code']);
          $res = json_decode($res,true);
          $strategy['nowprice']=$res[0]['nowPri'];
        }

        //已平仓的按照平仓价格计算
        if($strategy['status']==3||$strategy['status']==4){
            //涨/跌幅比率
            $strategy['rate'] = round((($strategy['sell_price']-$strategy['buy_price'])/$strategy['buy_price']*100),2).'%';

            //策略盈亏(收益)
            $strategy['income'] = round($strategy['sell_price']*$strategy['stock_number']-$strategy['market_value'],2);

            if($strategy['sell_price']>$strategy['buy_price']){
                //盈利分配
                $strategy['getprice']=round(($strategy['sell_price']*$strategy['stock_number']-$strategy['market_value'])*$chengshu,2);
                //亏损扣减
                $strategy['dropprice']='';
            }else{
                //盈利分配
                $strategy['getprice']='';
                //亏损扣减
                $strategy['dropprice']=round($strategy['true_getmoney']-$strategy['credit']-$strategy['credit_add']);
            }
        }else{
            //持仓中的没有盈利分配，亏损扣减
            //策略盈亏(收益)
            $strategy['income'] = round($nowprice*$strategy['stock_number']-$strategy['market_value'],2);

            //涨/跌幅比率
            $strategy['rate'] = round((($nowprice-$strategy['buy_price'])/$strategy['buy_price']*100),2).'%';
            //盈利分配
            $strategy['getprice']='';
            //亏损扣减
            $strategy['dropprice']='';
        }
        //策略创建时间
        $strategy['buy_time']=date('Y-m-d H:i:s',$strategy['buy_time']);
        if($strategy['sell_time']){
            //卖出时间
            $strategy['sell_time']=date('Y-m-d H:i:s',$strategy['sell_time']);
        }
        //策略状态
        if($strategy['status']==1){
            $strategy['status']='持仓申报中';
            $strategy['cancel']=1;
        }elseif($strategy['status']==2){
            $strategy['status']='持仓中';
            $strategy['cancel']=1;
        }elseif($strategy['status']==3){
            $strategy['status']='平仓申报中';
            $strategy['cancel']=2;
        }elseif($strategy['status']==4){
            $strategy['status']='已平仓';
            $strategy['cancel']=2;
        }
        //是否开启自动递延 1 开启 2未开启
        if($strategy['defer']==1){
            $strategy['defer']='开启';
        }else{
            $strategy['defer']='未开启';
        }
        //策略种类  T+1 T+5
        foreach(config('strategy_type') as $k=>$v){
            if($strategy['strategy_type']==$v){$strategy['strategy_type']=$k;}
        }
        $this->assign('strategy',$strategy);

        //协议
        $agreement = Db::name('agreement')->where(['strategy_num'=>$strategy['strategy_num']])->value('id');
        $this->assign('agreement',$agreement);

        return $this->fetch();
    }
    //已平仓的卖出价格修正
    function update_sell_price(){
        $info = Request::instance()->param();

        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        $nowprice=$info['price'];
        $id=$info['id'];
        if(array_key_exists('sell_time',$info)&&$info['sell_time']){
            $sell_time=$info['sell_time'];
            $data['sell_time']=$sell_time;
            $content['create_time']=$sell_time;
        }

        $strategy=Db::name('strategy')
            ->where(['is_cancel_order'=>1])
            ->where(['id'=>$id])
            ->find();

        $data['sell_price']=$nowprice;
        $get_money=$nowprice*$strategy['stock_number']-$strategy['market_value'];
        if($get_money>0){
            $data['true_getmoney']=$get_money*$chengshu+$strategy['credit']+$strategy['credit_add']-$strategy['sell_poundage'];
        }else{
            $data['true_getmoney']=$get_money+$strategy['credit']+$strategy['credit_add']-$strategy['sell_poundage'];
        }

        Db::startTrans();
        //修改策略记录
        $result1=Db::name('strategy')->where(['id'=>$id])->update($data);
        //修改用户策略余额
        $admin=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->find();
        $result2=Db::name('admin')
            ->where(['account' => $strategy['account'],'is_del'=>1])
            ->update(['tactics_balance'=>$admin['tactics_balance']-$strategy['true_getmoney']+$data['true_getmoney']]);
        //修改交易记录
        $content['trade_price']=$data['true_getmoney'];
        $result4=Db::name('trade')
            ->where(['charge_num'=>$strategy['strategy_num'],'is_del'=>1,'trade_status'=>1,'trade_type'=>'策略卖出'])
            ->update($content);
        if(!$result1 || !$result2 || !$result4){
            Db::rollback();
        }
        Db::commit();
    }
    //撤单
    function cancelstrategy(){
        $list = Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>input('id')])->find();
        $admin=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->find();
        $trade=Db::name('trade')->where(['charge_num' => $list['strategy_num'],'trade_type'=>'策略创建','is_del'=>1])->find();

        Db::startTrans();
        //修改用户策略余额和冻结金额
        $result1=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->update(['tactics_balance'=>$admin['tactics_balance']+$trade['trade_price']]);
        $result2=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->update(['frozen_money'=>$admin['frozen_money']-$list['credit']-$list['credit_add']]);
        //删除策略
        $result3=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $list['id']])->delete();
        //删除交易记录
        $result4=Db::name('trade')->where(['id' => $trade['id']])->delete();
        //删除订单协议
        $result5=Db::name('agreement')->where(['strategy_num'=>$list['strategy_num']])->delete();
        if(!$result1 || !$result2 || !$result3 || !$result4 || !$result5){
            Db::rollback();
            $this->error('撤单失败');
        }
        Db::commit();

        $this->success('撤单成功', 'strategy/index');
//        $json_data=chedanMessage(date('m-d',$list['buy_time']),' '.$list['stock_name'],$list['account']);
//        $array = json_decode($json_data, true);
//        if ($array['code'] == 0) {
//            $this->success('撤单成功', 'strategy/index');
//        } else {
//            $this->error('撤单失败');
//        }
    }
    //回退至持股
    function backstrategy(){
        $list = Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>input('id')])->find();
        $admin=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->find();
        $trade=Db::name('trade')->where(['charge_num' => $list['strategy_num'],'trade_type'=>'策略卖出','is_del'=>1])->find();

        Db::startTrans();
        //修改用户策略余额和冻结金额
        $result1=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->update(['tactics_balance'=>$admin['tactics_balance']-$trade['trade_price']]);
        $result2=Db::name('admin')->where(['account' => $list['account'],'is_del'=>1])->update(['frozen_money'=>$admin['frozen_money']+$list['credit']+$list['credit_add']]);
        //回退策略
        $data=[
            'status'=>2,
            'sell_type'=>'',
            'true_getmoney'=>'',
            'sell_price'=>'',
            'sell_contract_num'=>'',
            'all_poundage'=>$list['buy_poundage'],
            'sell_poundage'=>0,
            'sell_time'=>'',
        ];
        $result3=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $list['id']])->update($data);
        //删除交易记录
        $result4=Db::name('trade')->where(['id' => $trade['id']])->delete();
        if(!$result1 || !$result2 || !$result3 || !$result4 ){
            Db::rollback();
            $this->error('回退至持股失败');
        }
        Db::commit();

        $this->success('回退至持股成功', 'strategy/index');
//        $json_data=chedanMessage(date('m-d',$list['buy_time']),' '.$list['stock_name'],$list['account']);
//        $array = json_decode($json_data, true);
//        if ($array['code'] == 0) {
//            $this->success('回退至持股成功', 'strategy/index');
//        } else {
//            $this->error('回退至持股失败');
//        }
    }

    //策略导出
    function exportstrategy(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }

        $info = Request::instance()->param();

        //搜索中的策略种类查询
        if(array_key_exists('strategy_type',$info)&&$info['strategy_type']){
            $where['str.strategy_type'] = $info['strategy_type'];
            $this->assign('strategy_typed',$info['strategy_type']);
        }
        //搜索中的策略状态查询
        if(array_key_exists('status',$info)&&$info['status']){
            $where['str.status'] = $info['status'];
            $this->assign('statusd',$info['status']);
        }
        //搜索中的是否实盘查询
        if(array_key_exists('is_real_disk',$info)&&$info['is_real_disk']){
            $where['str.is_real_disk'] = $info['is_real_disk'];
            $this->assign('is_real_disked',$info['is_real_disk']);
        }
        //统计数据跳转过来的账号查询
        if(array_key_exists('account',$info)&&$info['account']){
            $where['ad.account'] = $info['account'];
            $this->assign('account',$info['account']);
        }
        //统计数据跳转过来的分销商查询
        if(array_key_exists('mark',$info)&&$info['mark']){
            $where['ad.marketer_id'] = $info['mark'];
            $this->assign('mark',$info['mark']);
        }
        //统计数据跳转过来的时间查询（天周月）1为当天 2为当周 3为当月
        if(array_key_exists('date',$info)&&$info['date']==1){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==2){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime("this week Monday")];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==3){
            if(array_key_exists('status',$info)&&$info['status']==2){
                $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            }else{
                $strategywhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            }
            $this->assign('date',$info['date']);
        }elseif(array_key_exists('date',$info)&&$info['date']==4){
            $strategywhere=[];
            $this->assign('date',$info['date']);
        }else{
            $strategywhere=[];
        }


        $list = Db::name('strategy')
            ->alias('str')
            ->field('str.id,str.strategy_num,str.account,str.strategy_type,str.status,str.sell_type,str.stock_name,str.stock_code,
                str.stock_number,str.credit,str.credit_add,str.true_getmoney,str.market_value,str.double_value,str.buy_price,str.sell_price,str.surplus_value,
                str.loss_value,str.is_packet,str.is_real_disk,str.shipan_account,str.is_cancel_order,str.buy_contract_num,str.sell_contract_num,
                str.all_poundage,str.buy_poundage,str.sell_poundage,str.defer,str.defer_value,str.defer_day,str.rest_day,str.buy_time,
                str.sell_time,ad.true_name')
            ->join('link_admin ad','ad.account=str.account')
            ->where($where)
            ->where($strategywhere)
            ->where(['str.is_cancel_order'=>1])
            ->select();

        if($list){
            //获取当前单价
            $strategyarr=[];
            $cishu=ceil(count($list)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($list as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $kk=>$vv){
                    $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
                }
            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $list[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }

            foreach($list as $k=>$v){
                //策略种类  T+1 T+5
                foreach(config('strategy_type') as $kk=>$vv){
                    if($v['strategy_type']==$vv){$list[$k]['strategy_type']=$kk;}
                }
                //已平仓的按照平仓价格计算
                if($v['status']==3||$v['status']==4){
                    //策略盈亏(收益)
                    $list[$k]['income'] = round($v['sell_price']*$v['stock_number']-$v['market_value'],2);
                    //涨/跌幅比率
                    $list[$k]['rate'] = round((($v['sell_price']-$v['buy_price'])/$v['buy_price']*100),2).'%';
                    //持仓时间
                    $list[$k]['chicang_day'] = round((strtotime(date('Ymd',$v['sell_time']))-strtotime(date('Ymd',$v['buy_time'])))/3600/24)-$v['rest_day'];
                }else{
                    //策略盈亏(收益)
                    $list[$k]['income']=0;
                    //涨/跌幅比率
                    $list[$k]['rate'] = '0%';
                    //持仓时间
                    $list[$k]['chicang_day'] = 0;
//                    //策略盈亏(收益)
//                    $list[$k]['income']=round($v['nowprice']*$v['stock_number']-$v['market_value'],2);
//                    //涨/跌幅比率
//                    $list[$k]['rate'] = round((($v['nowprice']-$v['buy_price'])/$v['buy_price']*100),2).'%';
//                    //持仓时间
//                    $list[$k]['chicang_day'] = round((strtotime(date('Ymd'))-strtotime(date('Ymd',$v['buy_time'])))/3600/24)-$v['rest_day'];
                }
                //卖出类型
                if($v['sell_type']==1){
                    $list[$k]['sell_type']='主动卖出';
                }elseif($v['sell_type']==2){
                    $list[$k]['sell_type']='达到日期';
                }elseif($v['sell_type']==3){
                    $list[$k]['sell_type']='触发止盈';
                }elseif($v['sell_type']==4){
                    $list[$k]['sell_type']='触发止损';
                }
                //策略状态
                if($v['status']==1){
                    $list[$k]['status']='持仓申报中';
                }elseif($v['status']==2){
                    $list[$k]['status']='持仓中';
                }elseif($v['status']==3){
                    $list[$k]['status']='平仓申报中';
                }elseif($v['status']==4){
                    $list[$k]['status']='已平仓';
                }
                //是否测试
                if($v['is_real_disk']==1){
                    $list[$k]['is_real_disk']='正式';
                }elseif($v['is_real_disk']==2){
                    $list[$k]['is_real_disk']='测试';
                }
                //是否撤单
                if($v['is_cancel_order']==1){
                    $list[$k]['is_cancel_order']='未撤单';
                }elseif($v['is_cancel_order']==2){
                    $list[$k]['is_cancel_order']='撤单中';
                }elseif($v['is_cancel_order']==3){
                    $list[$k]['is_cancel_order']='已撤单';
                }
                //是否使用红包
                if($v['is_packet']==1){
                    $list[$k]['is_packet']='使用';
                }elseif($v['is_packet']==2){
                    $list[$k]['is_packet']='未使用';
                }
                //是否开启自动递延
                if($v['defer']==1){
                    $list[$k]['defer']='开启';
                }elseif($v['defer']==2){
                    $list[$k]['defer']='未开启';
                }
                //买入时间
                if($v['buy_time']){
                    $list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
                }
                //卖出时间
                if($v['sell_time']){
                    $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
                }
            }

            //搜索中的策略盈亏查询
            if(array_key_exists('income',$info)&&$info['income']){
                if($info['income']==1){
                    foreach ($list as $k => $v) {
                        if($v['income']<0){unset($list[$k]);}
                    }
                }else{
                    foreach ($list as $k => $v) {
                        if($v['income']>=0){unset($list[$k]);}
                    }
                }
                $this->assign('incomed',$info['income']);
            }
        }else{
            $list=[];
        }

        //列表中的字段
        $last=[];
        foreach($list as $k=>$v){
            $last[$k]['strategy_num']=$v['strategy_num'];
            $last[$k]['account']=$v['account'];
            $last[$k]['true_name']=$v['true_name'];
            $last[$k]['stock_code']=$v['stock_code'];
            $last[$k]['stock_name']=$v['stock_name'];
            $last[$k]['stock_number']=$v['stock_number'];
            $last[$k]['strategy_type']=$v['strategy_type'];
            $last[$k]['buy_price']=$v['buy_price'];
            $last[$k]['nowprice']=$v['nowprice'];
            $last[$k]['income']=$v['income'];
            $last[$k]['rate']=$v['rate'];
            $last[$k]['defer_day']=$v['defer_day'];
            $last[$k]['status']=$v['status'];
        }

        //筛选条件
        $shuzu=[];
        if(array_key_exists('content',$info)&&$info['content']){
            //所有会员中筛选要导出的数据
            foreach ($last as $k => $v) {
                foreach ($v as $kk => $vv) {
                    if(strpos($vv,$info['content'])||strpos($vv,$info['content'])===0){$shuzu[$k]=$v;};
                }
            }
        }else{
            $shuzu=$list;
        }

        //在获取到的列表中选择搜索框的like
        $zuihou=[];
        foreach($list as $k=>$v){
            foreach($shuzu as $kk=>$vv){
                if($k==$kk){
                    $zuihou[]=$v;
                }
            }
        }

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($zuihou as $k => $v) {
            $zuihou[$k]['strategy_num']=$v['strategy_num'].' ';
        }

        $title='策略信息表';
        $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
        ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '追加的信用金', '返还至策略余额的钱'
        ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
        ,'是否测试', '正式账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
        ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
        ,'卖出时间', '真实姓名', '现价', '策略盈亏', '涨/跌幅比率', '持仓天数');
        daochu($title,$cellName,$zuihou);
    }


}
