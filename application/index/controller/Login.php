<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Login extends Controller
{

    public function header(){
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
        return $this->fetch();
    }
    public function footer(){
        return $this->fetch();
    }
    public function login(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            $data['account']=$info['account'];
            $data['pwd']=$info['pwd'];

            $rule = [
                'account' =>'require',
                'pwd' =>'require',
            ];
            $msg = [
                'account' =>  '用户名不能为空',
                'pwd' =>  '密码不能为空',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($data)){
                $this->error($validate->getError());
            }

            $data['pwd']=md5($data['pwd']);
            //查询管理员表是否能成功登陆
            $res = Db::name('user')->where($data)->find();
            if(!$res){
                //查询分销商表是否能成功登陆
                $res = Db::name('marketer')->where($data)->find();
                if(!$res){
                    //查询分销商工作人员表是否能成功登陆
                    $res = Db::name('marketer_user')->where($data)->find();
                    if($res){
                        $is_del = Db::name('marketer')->where(['id'=>$res['marketer_id']])->value('is_del');
                        if($is_del==2){$this->error('您所在的分销商已禁用');}
                    }
                    if(!$res){
                        $this->error('用户名或密码错误');
                    }elseif($res['is_del']==2){
                        $this->error('此工作人员账号已禁用');
                    }else{
                        //记录用户账号，名字，是分销商的员工
                        cookie('marketer',$res['marketer_id']);
                        cookie('marketer_user',$res['id']);
                        cookie('stockaccount',$res['account']);
                        $this->success('登陆成功','index/index');
                    }
                }elseif($res['is_del']==2){
                    $this->error('此分销商已禁用');
                }else{
                    //记录用户账号，名字，是分销商
                    cookie('marketer',$res['id']);
                    cookie('marketer_user',0);
                    cookie('stockaccount',$res['account']);
                    $this->success('登陆成功','index/index');
                }
            }elseif($res['is_del']==2){
                $this->error('此账号已删除');
            }else{
                //记录用户账号，名字，是管理员
                cookie('marketer',0);
                cookie('marketer_user',0);
                cookie('stockaccount',$res['account']);
                $this->success('登录成功','index/index');
            }
        }
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
      
        return $this->fetch();
    }
    //退出登录
    public function loginout(){
        //清除用户账号，名字
        cookie('stockaccount',null);
        cookie('stockuser',null);
        $this->redirect('login/login');
    }

  
  

}
