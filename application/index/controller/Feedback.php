<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Feedback extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    //用户反馈的问题
    public function index() {
        $list = Db::name('feedback')
            ->alias('f')
            ->field('ad.account,f.*')
            ->join('link_admin ad','ad.account=f.admin_id')
            ->where(['ad.is_del'=>1])
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            $list[$k]['content']=mb_substr($v['content'],0,50);
            //反馈类型，是否解决
            foreach(config('feedback_status') as $kk=>$vv){
                if($v['status']==$vv){$list[$k]['status']=$kk;}
            }
        }
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['status']=$info['status'];
            //更新反馈类型
            Db::name('feedback')->where(['id'=>$info['id']])->update($data);

            $this->success('修改成功','feedback/index');
        }
        $id=input('id');
        $feedback = Db::name('feedback')
            ->alias('f')
            ->field('ad.account,f.*')
            ->join('link_admin ad','ad.account=f.admin_id')
            ->where(['f.id'=>$id,'ad.is_del'=>1])
            ->find();
        $this->assign('feedback',$feedback);
        //反馈类型
        $this->assign('status',config('feedback_status'));

        return $this->fetch();
    }

    //用户反馈的问题的回复
    public function feedbackindex() {
        $info = Request::instance()->param();
        //查询反馈问题
        $feedback=Db::name('feedback')->where(['id'=>$info['id']])->find();
        //查询问题的所有回复
        $list=Db::name('feedback_talk')->where(['fid'=>$info['id']])->order('create_time asc')->select();

        $arr[0]['id']=0;
        $arr[0]['fid']=$feedback['id'];
        $arr[0]['account']=$feedback['admin_id'];
        $arr[0]['content']=$feedback['content'];
        $arr[0]['status']=1;
        $arr[0]['create_time']=date('Y-m-d H:i:s',$feedback['create_time']);
        foreach ($list as $k => $v) {
            $arr[$k+1]=$v;
            if($v['status']==2){
                $arr[$k+1]['account']='客服';
            }
            $arr[$k+1]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
        }

        $this->assign('arr',$arr);
        $this->assign('fid',$info['id']);
        return $this->fetch();
    }
    //对用户反馈的问题进行回复
    public function feedbacktalk() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            $data['fid']=$info['fid'];
            $data['account']=cookie('stockaccount');
            $data['content']=$info['content'];
            $data['status']=2;
            $data['create_time']=time();
            Db::name('feedback_talk')->insert($data);

            $this->success('回复成功',url('feedback/feedbackindex',['id'=>$info['fid']]));
        }
        $this->assign('fid',input('fid'));
        return $this->fetch();
    }


}
