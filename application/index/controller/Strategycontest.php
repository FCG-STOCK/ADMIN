<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Strategycontest extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index() {
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')&&(!cookie('marketer_user'))){
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }

        $info = Request::instance()->param();
        //搜索中的策略状态查询
        if(array_key_exists('status',$info)&&$info['status']){
            $where['str.status'] = $info['status'];
            $this->assign('statusd',$info['status']);
        }

        $list = Db::name('strategy_contest')
            ->alias('str')
            ->field('ad.account,str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($where)
            ->select();
        if($list){
            //获取当前单价
            $strategyarr=[];
            $cishu=ceil(count($list)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($list as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $kk=>$vv){
                    $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
                }
            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $list[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }

            foreach($list as $k=>$v){
                //已平仓的按照平仓价格计算
                if($v['status']==3||$v['status']==4){
                    //策略盈亏(收益)
                    $list[$k]['income'] = round(($v['sell_price']-$v['buy_price'])*$v['stock_number'],2);
                    //涨/跌幅比率
                    $list[$k]['rate'] = round((($v['sell_price']-$v['buy_price'])/$v['buy_price']*100),2).'%';
                }else{
                    //策略盈亏(收益)
                    $list[$k]['income'] = round(($v['nowprice']-$v['buy_price'])*$v['stock_number'],2);
                    //涨/跌幅比率
                    $list[$k]['rate'] = round((($v['nowprice']-$v['buy_price'])/$v['buy_price']*100),2).'%';
                }

                //策略状态
                if($v['status']==1){
                    $list[$k]['status']='持仓申报中';
                }elseif($v['status']==2){
                    $list[$k]['status']='持仓中';
                }elseif($v['status']==3){
                    $list[$k]['status']='平仓申报中';
                }elseif($v['status']==4){
                    $list[$k]['status']='已平仓';
                }
            }
            //搜索中的策略盈亏查询
            if(array_key_exists('income',$info)&&$info['income']){
                if($info['income']==1){
                    foreach ($list as $k => $v) {
                        if($v['income']<0){unset($list[$k]);}
                    }
                }else{
                    foreach ($list as $k => $v) {
                        if($v['income']>=0){unset($list[$k]);}
                    }
                }
                $this->assign('incomed',$info['income']);
            }
        }else{
            $list=[];
        }

        $this->assign('list',$list);
        return $this->fetch();
    }
    //策略详情
    public function detail(){
        //策略
        $strategy = Db::name('strategy_contest')
            ->alias('str')
            ->field('ad.account,str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where(['str.id'=>input('id')])
            ->find();
        //市值
        $strategy['market_value']=$strategy['buy_price']*$strategy['stock_number'];

        //股票当前的详细数据
        $json = getOneStock($strategy['stock_code']);
        $stock = json_decode($json, true);
        //股票当前价格
        $nowprice = $stock['result'][0]['data']['nowPri'];
        $strategy['nowprice']=$nowprice;

        //已平仓的按照平仓价格计算
        if($strategy['status']==3||$strategy['status']==4){
            //涨/跌幅比率
            $strategy['rate'] = round((($strategy['sell_price']-$strategy['buy_price'])/$strategy['buy_price']*100),2).'%';
            //策略盈亏(收益)
            $strategy['income'] = round(($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'],2);
            if($strategy['income']>0){
                //盈利分配
                $strategy['getprice']=$strategy['income'];
                //亏损扣减
                $strategy['dropprice']='';
            }else{
                //盈利分配
                $strategy['getprice']='';
                //亏损扣减
                $strategy['dropprice']=$strategy['income'];
            }
        }else{
            //持仓中的没有盈利分配，亏损扣减
            //策略盈亏(收益)
            $strategy['income'] = round($nowprice*$strategy['stock_number']-$strategy['credit'],2);
            //涨/跌幅比率
            $strategy['rate'] = round((($nowprice-$strategy['buy_price'])/$strategy['buy_price']*100),2).'%';
            //盈利分配
            $strategy['getprice']='';
            //亏损扣减
            $strategy['dropprice']='';
        }
        //策略创建时间
        $strategy['buy_time']=date('Y-m-d H:i:s',$strategy['buy_time']);
        if($strategy['sell_time']){
            //卖出时间
            $strategy['sell_time']=date('Y-m-d H:i:s',$strategy['sell_time']);
        }
        //策略状态
        if($strategy['status']==1){
            $strategy['status']='持仓申报中';
        }elseif($strategy['status']==2){
            $strategy['status']='持仓中';
        }elseif($strategy['status']==3){
            $strategy['status']='平仓申报中';
        }elseif($strategy['status']==4){
            $strategy['status']='已平仓';
        }

        $this->assign('strategy',$strategy);
        return $this->fetch();
    }


}
