<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Stock extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }
        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }
    public function index() {
        $list = Db::name('stock_recommend')->select();
        //股票推荐状态
        foreach($list as $k=>$v){
            if($v['is_recommend']==1){
                $list[$k]['is_recommend']='已推荐';
            }else{
                $list[$k]['is_recommend']='未推荐';
            }
        };
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function add(){
        if (Request::instance()->isPost()) {

            if(!input('stock_code')){
                $this->error('股票代码不能为空');
            }
            $res=Db::name('stock_recommend')->where(['stock_code'=>input('stock_code')])->find();
            if($res){
                $this->error('股票已经添加过了');
            }

            //查找股票详情
            $url = 'http://web.juhe.cn:8080/finance/stock/hs';
            $data['key'] = '7fdd610575a30a240bba1b9cd7529896';
            $data['gid'] = input('stock_code');
            $res = https_request($url, '', $data);
            $stock = json_decode($res, true);
            if($stock['resultcode']==200){
                $insert['stock_code']=$stock['result'][0]['data']['gid'];
                $insert['stock_name']=$stock['result'][0]['data']['name'];
            }else{
                $this->error('股票未找到');
            }

            $res = Db::name('stock_recommend')->insert($insert);
            if($res){
                $this->success('推荐成功','stock/index');
            }else{
                $this->error('推荐失败');
            }
        }
        return $this->fetch();
    }
    public function update() {
        if (Request::instance()->isPost()) {
            $info = Request::instance()->param(true);

            //股票修改推荐
            $data['is_recommend']=$info['is_recommend'];
            Db::name('stock_recommend')->where(['id'=>$info['id']])->update($data);

            $this->success('修改成功','stock/index');
        }
        $id=input('id');
        $stockdetail = Db::name('stock_recommend')
            ->where(['id'=>$id])
            ->find();
        $this->assign('stockdetail',$stockdetail);
        return $this->fetch();
    }

}
