<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Trade extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
            $this->assign('del',$user['is_del']);
            $this->assign('marketer_id',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }

    //交易记录
    public function index() {
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
        }else{
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }
        $where['ad.is_del']=1;
        $where['tr.is_del']=1;

        $info = Request::instance()->param();
        //搜索中的订单单号查询
        if(array_key_exists('charge_num',$info)&&$info['charge_num']){
            $where['tr.charge_num'] = ['like',['%'.$info['charge_num'].'%']];
            $this->assign('charge_numed',$info['charge_num']);
        }
        //搜索中的交易账号查询
        if(array_key_exists('account',$info)&&$info['account']){
            $where['tr.account'] = ['like',['%'.$info['account'].'%']];
            $this->assign('accounted',$info['account']);
        }
        //搜索中的真实姓名查询
        if(array_key_exists('true_name',$info)&&$info['true_name']){
            $where['ad.true_name'] = ['like',['%'.$info['true_name'].'%']];
            $this->assign('true_nameed',$info['true_name']);
        }

        $list = Db::name('trade')
            ->alias('tr')
            ->field('ad.id as num,tr.*,ad.true_name')
            ->join('link_admin ad','ad.account=tr.account')
            ->where($where)
            ->order('id desc')
            ->paginate(10, false, ['query' => request()->param()])
            ->each(function ($item, $key) {
                $item['create_time'] = date('Y-m-d H:i:s', $item['create_time']);
                if ($item['trade_type'] == '提现') {
                    if ($item['trade_status'] == 1) {
                        $item['trade_status'] = '提现成功';
                    } elseif ($item['trade_status'] == 2) {
                        $item['trade_status'] = '提现失败';
                    } elseif ($item['trade_status'] == 3) {
                        $item['trade_status'] = '提现中';
                    } elseif ($item['trade_status'] == 4) {
                        $item['trade_status'] = '拒绝提现';
                    } elseif ($item['trade_status'] == 5) {
                        $item['trade_status'] = '提现申请中';
                    }
                } else {
                    if ($item['trade_status'] == 1) {
                        $item['trade_status'] = '已付款';
                    } elseif ($item['trade_status'] == 2) {
                        $item['trade_status'] = '未付款';
                    }
                }
                return $item;
            });


        $this->assign('list',$list);
        $this->assign('marketer_user',cookie('marketer_user'));
        return $this->fetch();
    }

    //导出交易记录
    public function exporttrade() {
        $info = Request::instance()->param();

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
        }else{
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }
        $where['ad.is_del']=1;
        $where['tr.is_del']=1;

        //搜索中的订单单号查询
        if(array_key_exists('charge_num',$info)&&$info['charge_num']){
            $where['tr.charge_num'] = ['like',['%'.$info['charge_num'].'%']];
        }
        //搜索中的交易账号查询
        if(array_key_exists('account',$info)&&$info['account']){
            $where['tr.account'] = ['like',['%'.$info['account'].'%']];
        }
        //搜索中的真实姓名查询
        if(array_key_exists('true_name',$info)&&$info['true_name']){
            $where['ad.true_name'] = ['like',['%'.$info['true_name'].'%']];
        }

        $list = Db::name('trade')
            ->alias('tr')
            ->field('tr.*,ad.id as num,ad.true_name')
            ->join('link_admin ad','ad.account=tr.account')
            ->where($where)
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            if($v['trade_type']=='提现'){
                if($v['trade_status']==1){
                    $list[$k]['trade_status']='提现成功';
                }elseif($v['trade_status']==2){
                    $list[$k]['trade_status']='提现失败';
                }elseif($v['trade_status']==3){
                    $list[$k]['trade_status']='提现中';
                }elseif($v['trade_status']==4){
                    $list[$k]['trade_status']='拒绝提现';
                }elseif($v['trade_status']==5){
                    $list[$k]['trade_status']='提现申请中';
                }
            }else{
                if($v['trade_status']==1){
                    $list[$k]['trade_status']='已付款';
                }elseif($v['trade_status']==2){
                    $list[$k]['trade_status']='未付款';
                }
            }
            //是否删除
            if($v['is_del']==1){
                $list[$k]['is_del']='是';
            }elseif($v['is_del']==2){
                $list[$k]['is_del']='否';
            }
            //交易发生时间
            if($v['create_time']){
                $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            }
        };

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($list as $k => $v) {
            $list[$k]['detailed']=$v['detailed'].' ';
            $list[$k]['charge_num']=$v['charge_num'].' ';
        }

        $title='交易记录信息表';
        $cellName = array('ID', '用户账号', '交易单号', '交易金额', '提现手续费'
        ,'剩余金额', '交易类型', '交易状态', '备注', '排序'
        ,'是否删除', '交易发生时间', '账号ID', '真实姓名');
        daochu($title,$cellName,$list);
    }

    //充值页面看到的会员列表
    public function chongzhi(){
        $list = Db::name('admin')->where(['is_del'=>1])->select();
        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['lastlogin_time']=date('Y-m-d',$v['lastlogin_time']);
            if($v['is_true']==1){
                $list[$k]['is_true']='已认证';
            }elseif($v['is_true']==2){
                $list[$k]['is_true']='认证失败';
            }elseif($v['is_true']==3){
                $list[$k]['is_true']='待审核';
            }else{
                $list[$k]['is_true']='未提交';
            }
        }
        $this->assign('list',$list);
        return $this->fetch();
    }

    //进行充值
    public function createchongzhi(){
    	//LRSS 2019-2-17 ADD 权限判断
    	//查询当前用户的权限数组
    	$user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
    	if(empty($user)){
    		$this->error('无操作权限');
    	}else{
    		$auth_array = explode(',', trim($user['auth'],',')); //获得权限数组
    		if(!in_array('14', $auth_array)){
    			$this->error('无操作权限');
    		}
    	}
    	
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            if(!$info['balance']){
                $this->error('充值金额不能为空');
            }

            //LRSS 2019-2-16 ADD 添加充值备注
            //$info['detailed'] = $user['username'].' - '.$info['detailed'];
            //$rule = [
	         //   'balance' =>  'require|number',
	         //   'detailed' =>  'max:255',
           // ];
           // $msg = [
	           // 'balance.require' =>  '充值金额不能为空',
	           // 'balance.number' =>  '请输入正确的充值金额',
	          //  'detailed' =>  '备注信息太长',
           // ];
           // $validate = new Validate($rule, $msg);
           // if(!$validate->check($info)){
            //	$this->error($validate->getError());
           // }
            
            $admin=Db::name('admin')->where(['id'=>$info['id'],'is_del'=>1])->find();

            Db::name('admin')->where(['id'=>$info['id'],'is_del'=>1])->update(['balance'=>$admin['balance']+$info['balance']]);

            //添加充值成功的记录
            $para=[
                'account'=>$admin['account'],
                'charge_num'=>date('YmdHis').rand(1000,9999),
                'trade_price'=>$info['balance'],
                'trade_type'=>'后台充值',
                'trade_status'=>1,
                'sort'=>0,
                'is_del'=>1,
                'create_time'=>time(),
                
                //LRSS 2019-2-16 ADD 添加充值备注
               // 'detailed'=>$info['detailed']
            ];
            Db::name('trade')->insert($para);

            $this->success('充值成功', 'trade/chongzhi');
        }
        $id=input('id');
        $user=Db::name('admin')->where(['id'=>$id,'is_del'=>1])->find();
        $this->assign('user',$user);
        return $this->fetch();
    }

    //提现申请中的记录
    public function extractmoney() {
        $info = Request::instance()->param(true);

        $list = Db::name('trade')
            ->alias('tr')
            ->field('ad.id as num,tr.*,ad.true_name,bank.bank_name,bank.bank_num,bank.bank_address')
            ->join('link_admin ad','ad.account = tr.account')
            ->join('link_bankcart bank','bank.account = tr.account')
            ->where(['tr.trade_type'=>'提现','tr.trade_status'=>$info['status'],'tr.is_del'=>1,'bank.bank_require'=>1,'bank.is_del'=>1])
            ->select();

        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            if($v['trade_status']==1){
                $list[$k]['trade_status']='提现成功';
            }elseif($v['trade_status']==2){
                $list[$k]['trade_status']='提现失败';
            }elseif($v['trade_status']==3){
                $list[$k]['trade_status']='提现中';
            }elseif($v['trade_status']==4){
                $list[$k]['trade_status']='拒绝提现';
            }elseif($v['trade_status']==5){
                $list[$k]['trade_status']='提现申请中';
            }
        };
        $this->assign('list',$list);

        $this->assign('status',$info['status']);
        return $this->fetch();
    }
    //同意提现
    public function agreeExtract() {
        $info = Request::instance()->param(true);

        if(!$info['id']){
            return ['status'=>0,'msg'=>'请选择订单'];
        }

        //提现手续费
        $tixian=Db::name('hide')->where(['id'=>10])->value('value');

      	//修改为提现完成
        Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
            ->update(['tr.trade_status'=>1]);
      
      	$trade=Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
          	->select();
      	foreach($trade as $k=>$v){
            push($v['account'],'恭喜您，您的提现申请已经审核通过！',1);
        }
      	
      	return ['status'=>1,'msg'=>'提现成功'];
      
        //修改为提现中
        Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
            ->update(['tr.trade_status'=>3]);

        //选中的提现订单
        $list=Db::name('trade')
            ->field('ad.true_name,tr.*,bank.bank_num')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->join('link_bankcart bank','ad.account=bank.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1,'bank.bank_require'=>1,'bank.is_del'=>1])
            ->select();

        //同意提现手续费的订单
        foreach($list as $k=>$v){
            Db::name('trade')->where(['charge_num'=>$v['detailed']])->update(['trade_status'=>1]);
        }

        foreach ($list as $k => $v) {
            //组装连连订单数组
            $data = [
                //账号
                'account'    => $v['account'],
                //订单号
                'charge_num' => $v['charge_num'],
                //付款金额
                'money'      => $v['trade_price'] * 100,
                //收款人姓名
                'true_name'  => $v['true_name'],
                //收款银行账号
                'bank_num'   => $v['bank_num'],
            ];

            //进行提现
            $this->tixianMoney($data);
        }
        return ['status'=>1,'msg'=>'提现成功'];
    }
    //提现
    public function tixianMoney($data){
        $ver="2.00";
        $amt=$data['money'];
        $cityno="";
        $entseq="";
        $bankno="";
        $merdt=date("Ymd");
        $accntno=$data['bank_num'];
        $orderno=$data['charge_num'];
        $branchnm="";
        $accntnm=$data['true_name'];
        $mobile=$data['account'];
        $memo="";
        $mchntcd="0005840F2143070";
        $mchntkey="l20hiieqqmfd8sl957p6m57los2j18o0";
        $reqtype="payforreq";

        $xml="<?xml version='1.0' encoding='utf-8' standalone='yes'?><payforreq><ver>".$ver."</ver><merdt>".$merdt."</merdt><orderno>".$orderno."</orderno><bankno>".$bankno."</bankno><cityno>".$cityno."</cityno><accntno>".$accntno."</accntno><accntnm>".$accntnm."</accntnm><branchnm>".$branchnm."</branchnm><amt>".$amt."</amt><mobile>".$mobile."</mobile><entseq>".$entseq."</entseq><memo>".$memo."</memo></payforreq>";
        $macsource=$mchntcd."|".$mchntkey."|".$reqtype."|".$xml;
        $mac=md5($macsource);
        $mac=strtoupper($mac);
        $list=array("merid"=>$mchntcd,"reqtype"=>$reqtype,"xml"=>$xml,"mac"=>$mac);
        $url="https://fht-api.fuioupay.com/req.do";
        $query = http_build_query($list);
        $options = array(
            'http' => array(
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
                'method'  => "POST",
                'content' => $query,
            ),
        );
      
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context, -1, 40000);
      	
        return $result;
    }
    //拒绝提现
    public function refuseExtract() {
        $info = Request::instance()->param();
        if(!$info['id']){
            return ['status'=>0,'msg'=>'请选择订单'];
        }

        //提现手续费
        $tixian=Db::name('hide')->where(['id'=>10])->value('value');

        //选中的提现订单
        $list=Db::name('trade')
            ->field('tr.*,ad.balance')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
            ->select();
        foreach ($list as $k => $v) {
            Db::name('admin')->where(['account'=>$v['account'],'is_del'=>1])->update(['balance'=>$v['balance']+$tixian+$v['trade_price']]);
        }
        //修改为拒绝提现
        Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
            ->update(['tr.trade_status'=>4]);

        //LRSS 2019-2-26 ADD
        //所有提现的订单
        $arr=Db::name('trade')
            ->alias('tr')
            ->field('tr.*')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>['in',$info['id']],'ad.is_del'=>1])
            ->select();
        //拒绝提现手续费的订单
        foreach($arr as $k=>$v){
            Db::name('trade')->where(['charge_num'=>$v['detailed']])->update(['trade_status'=>4]);
        }
      	
      	foreach($list as $k=>$v){
            push($v['account'],'您的提现申请被拒绝，请联系客服进行询问',1);
        }
		
        return ['status'=>1,'msg'=>'拒绝成功'];
    }
  
  	//充值申请中的记录
    public function zhuanzhang() {
        $info = Request::instance()->param(true);

      	
      
        $list = Db::name('trade')
            ->alias('tr')
            ->field('ad.id as num,tr.*,ad.true_name')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.trade_type'=>['in',['银行卡转账','支付宝转账','微信转账']],'tr.trade_status'=>$info['status'],'tr.is_del'=>1])
          	->order('tr.create_time desc')
            ->select();
      


        foreach($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            if($v['trade_status']==1){
                $list[$k]['trade_status']='充值成功';
            }elseif($v['trade_status']==2){
                $list[$k]['trade_status']='待确认';
            }elseif($v['trade_status']==3){
                $list[$k]['trade_status']='拒绝充值';
            }
        };
        $this->assign('list',$list);

        $this->assign('status',$info['status']);
        return $this->fetch();
    }
  
  
  
  
  	//同意充值
    public function agree_zhuanzhang() {
        $info = Request::instance()->param(true);
      	

      
      	if(!$info['id']){
            return ['status'=>0,'msg'=>'请选择订单'];
        }
      
      	$id_arr = explode(',', $info['id']);
        unset($id_arr[count($id_arr)-1]);
      

        foreach ($id_arr as $k => $v) {
            $account = Db::name('trade')->where('id',$v)->value('account');
            $trade_price = Db::name('trade')->where('id',$v)->value('trade_price');
          	Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>$v,'ad.is_del'=>1])
            ->update(['tr.trade_status'=>1]);
          
            $res = Db::name('admin')->where('account',$account)->setInc('balance',$trade_price);
          
          	send_message_recharge($account);

        }
      
      	if($res){
          return ['status'=>1,'msg'=>'充值成功'];
        }else{
          return ['status'=>0,'msg'=>'充值失败'];
        }
      

        
    }
  
  
  	//拒绝充值
    public function refuse_zhuanzhang() {
        $info = Request::instance()->param(true);
      
      	if(!$info['id']){
            return ['status'=>0,'msg'=>'请选择订单'];
        }
      
      	$id_arr = explode(',', $info['id']);
        unset($id_arr[count($id_arr)-1]);
      

        foreach ($id_arr as $k => $v) {
            $account = Db::name('trade')->where('id',$v)->value('account');
            $trade_price = Db::name('trade')->where('id',$v)->value('trade_price');
          	$res = Db::name('trade')
            ->alias('tr')
            ->join('link_admin ad','ad.account=tr.account')
            ->where(['tr.id'=>$v,'ad.is_del'=>1])
            ->update(['tr.trade_status'=>3]);

          	send_message_recharge($account);

        }
      	if($res){
          return ['status'=>1,'msg'=>'操作成功'];
        }else{
          return ['status'=>0,'msg'=>'操作失败'];
        }
      	
    }
}
