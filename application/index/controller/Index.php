<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
class Index extends Controller
{
    public function __construct(Request $request) {
        parent::__construct($request);
        if(!cookie('stockaccount')){
            $this->error('请登陆','login/login');
        }
        //查询当前用户的权限数组
        $user=Db::name('user')->where(['account'=>cookie('stockaccount')])->find();
        if(!$user){
            $user=Db::name('marketer')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('is_del',$user['is_del']);
            $this->assign('marketer',$user['id']);
        }

        if(!$user){
            $user=Db::name('marketer_user')->where(['account'=>cookie('stockaccount')])->find();
            $this->assign('marketer',$user['id']);
        }
        $this->assign('username',$user['username']);
        $this->assign('userheadurl',$user['headurl']);
        $userauth=explode(',',substr($user['auth'],0,-1));
        $this->assign('userauth',$userauth);
      	//后台浏览器头部图片
        $web_url=Db::name('hide')->where(['id'=>39])->value('value');
        $this->assign('web_url',$web_url);
        //后台浏览器标题
        $web_title=Db::name('hide')->where(['id'=>40])->value('value');
        $this->assign('web_title',$web_title);
        //后台管理系统标题
        $back_system_title=Db::name('hide')->where(['id'=>41])->value('value');
        $this->assign('back_system_title',$back_system_title);
    }

    public function index() {
        //今日零点
        $today = strtotime(date("Y-m-d"),time());

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $today_where['ad.marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $today_where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $today_where['ad.marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        //今日新增记录
        $list=[];
        //今日新增会员数量
        $where['create_time']=['egt',$today];
        $where['is_del']=1;
        $person=Db::name('admin')->where($where)->count();
        $list['person']=$person;

        //今日新增的策略
        $today_where['str.buy_time']=['egt',$today];
        $today_strategy=Db::name('strategy')
            ->alias('str')
            ->field('str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where(['str.is_cancel_order'=>1])
            ->where($today_where)
            ->select();

        //今日新增策略个数
        $list['strategy_num']=count($today_strategy);
        //总市值
        $list['all_market_value']=0;
        //总信用金
        $list['all_credit']=0;
        foreach($today_strategy as $k=>$v){
            $list['all_market_value']+=$v['market_value'];
            $list['all_credit']+=$v['credit']+$v['credit_add'];
        }
        $this->assign('list',$list);

        //所有记录
        $history=[];
        //所有会员数量
        unset($where['create_time']);
        $alladmin=Db::name('admin')->where($where)->select();
        $history['person']=count($alladmin);

        //所有会员的所有策略
        $history_strategy=Db::name('strategy')
            ->alias('str')
            ->field('str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($history_where)
            ->where(['str.is_cancel_order'=>1])
            ->select();
        if($history_strategy){
            foreach ($history_strategy as $k => $v) {
                if($v['strategy_type']==1){
                    $history_strategy[$k]['strategy_type']='T+1';
                }else{
                    $history_strategy[$k]['strategy_type']='T+5';
                }
            }
        }

        //所有会员的所有策略个数
        $history['strategy_num']=count($history_strategy);
        //总市值
        $history['all_market_value']=0;
        //总信用金
        $history['all_credit']=0;
        if($history_strategy){
            foreach($history_strategy as $k=>$v){
                $history['all_market_value']+=$v['market_value'];
                $history['all_credit']+=$v['credit']+$v['credit_add'];
            }
        }
        $this->assign('history',$history);

        //所有策略中的最新10条
        array_multisort(array_column($history_strategy,'buy_time'),SORT_DESC,$history_strategy);
        if(count($history_strategy)<=10){
            $strategy_ranking=$history_strategy;
        }else{
            for($i=0;$i<10;$i++){
                $strategy_ranking[$i]=$history_strategy[$i];
            }
        }
        $this->assign('strategy_ranking',$strategy_ranking);

        //所有会员的余额，策略余额，冻结金额的和
        foreach($alladmin as $k=>$v){
            $alladmin[$k]['allmoney']=$v['balance']+$v['tactics_balance']+$v['frozen_money'];
        }
        array_multisort(array_column($alladmin,'allmoney'),SORT_DESC,$alladmin);
        if(count($alladmin)<=10){
            $admin_ranking=$alladmin;
        }else{
            for($i=0;$i<10;$i++){
                $admin_ranking[$i]=$alladmin[$i];
            }
        }
        $this->assign('admin_ranking',$admin_ranking);

        $this->assign('marketer_user',cookie('marketer_user'));
        return $this->fetch();
    }
    //统计数据
    public function countinfo(){
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        $info = Request::instance()->param();

        //1为当天 2为当周 3为当月
        if($info['date']==1){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ymd'))];
            $adminwhere['create_time']=['gt',strtotime(date('Ymd'))];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ymd'))];
        }elseif($info['date']==2){
            $packetwhere['pac.create_time']=['gt',strtotime("this week Monday")];
            $adminwhere['create_time']=['gt',strtotime("this week Monday")];
            $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            $strategysellwhere['str.sell_time']=['gt',strtotime("this week Monday")];
            $tradewhere['tra.create_time']=['gt',strtotime("this week Monday")];
        }elseif($info['date']==3){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ym').'01')];
            $adminwhere['create_time']=['gt',strtotime(date('Ym').'01')];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ym').'01')];
        }else{
            $packetwhere=[];
            $adminwhere=[];
            $strategywhere=[];
            $strategysellwhere=[];
            $tradewhere=[];
        }
        $this->assign('date',$info['date']);

        if(array_key_exists('begin_time',$info)&&$info['begin_time']){
            $packetwhere['pac.create_time']=['gt',strtotime($info['begin_time'])];
            $adminwhere['create_time']=['gt',strtotime($info['begin_time'])];
            $strategywhere['str.buy_time']=['gt',strtotime($info['begin_time'])];
            $strategysellwhere['str.sell_time']=['gt',strtotime($info['begin_time'])];
            $tradewhere['tra.create_time']=['gt',strtotime($info['begin_time'])];
            $this->assign('begin_time',$info['begin_time']);
        }
        if(array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $adminwhere['create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategywhere['str.buy_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategysellwhere['str.sell_time']=['lt',strtotime($info['end_time'])+24*3600];
            $tradewhere['tra.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $this->assign('end_time',$info['end_time']);
        }
        if(array_key_exists('begin_time',$info)&&$info['begin_time']&&array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $adminwhere['create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategywhere['str.buy_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategysellwhere['str.sell_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $tradewhere['tra.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $this->assign('begin_time',$info['begin_time']);
            $this->assign('end_time',$info['end_time']);
        }
        //从会员表过来的时候带的手机号
        if(array_key_exists('account',$info)&&$info['account']){
            $packetwhere['pac.account']=$info['account'];
            $adminwhere['account']=$info['account'];
            $strategywhere['str.account']=$info['account'];
            $strategysellwhere['str.account']=$info['account'];
            $tradewhere['tra.account']=$info['account'];
            $this->assign('account',$info['account']);
        }
        //分销商
        if(array_key_exists('mark',$info)&&$info['mark']){
            $packetwhere['ad.marketer_id']=$info['mark'];
            $adminwhere['marketer_id']=$info['mark'];
            $strategywhere['ad.marketer_id']=$info['mark'];
            $strategysellwhere['ad.marketer_id']=$info['mark'];
            $tradewhere['ad.marketer_id']=$info['mark'];
            $this->assign('markk',$info['mark']);
        }

        //所有会员总共得到的红包
        $packetarr=Db::name('packet')
            ->alias('pac')
            ->field('pac.*')
            ->join('link_admin ad','pac.account=ad.account')
            ->where($history_where)
            ->where($packetwhere)
            ->where(['ad.is_del'=>1])
            ->select();
        if($packetarr){
            //已使用红包
            $packetedMoney=0;
            //所有红包
            $packetMoney=0;
            //已使用红包次数
            $packetedTimes=0;
            foreach($packetarr as $k=>$v){
                $packetMoney+=$v['packet_money'];
                if($v['is_use']==1){
                    $packetedMoney+=$v['packet_money'];
                    $packetedTimes+=1;
                }
                $packet['packetedMoney']=$packetedMoney;
                $packet['packetMoney']=$packetMoney;
                $packet['packetedTimes']=$packetedTimes;
            }
            //所有的红包个数
            $packet['packetTimes']=count($packetarr);
            //红包金额使用率
            $packet['packetMoneyLv']=round($packetedMoney/$packetMoney*100,2).'%';
            //红包次数使用率
            $packet['packetTimesLv']=round($packetedTimes/$packet['packetTimes']*100,2).'%';
        }else{
            $packet['packetedMoney']=0;
            $packet['packetMoney']=0;
            $packet['packetedTimes']=0;
            //所有的红包个数
            $packet['packetTimes']=0;
            //红包金额使用率
            $packet['packetMoneyLv']='0%';
            //红包次数使用率
            $packet['packetTimesLv']='0%';
        }


        //所有能看到的会员数量
        $alladmin=Db::name('admin')
            ->where($where)
            ->where($adminwhere)
            ->where(['is_del'=>1])
            ->select();
        $admin['person']=count($alladmin);
        //余额
        $balance=0;
        //策略余额
        $tactics_balance=0;
        //冻结金额
        $frozen_money=0;
        foreach($alladmin as $k=>$v){
            $balance+=$v['balance'];
            $tactics_balance+=$v['tactics_balance'];
            $frozen_money+=$v['frozen_money'];
        }
        $admin['balance']=$balance;
        $admin['tactics_balance']=$tactics_balance;
        $admin['frozen_money']=$frozen_money;
        $admin['all_balance']=$balance+$tactics_balance+$frozen_money;


        //所有能看到的会员的所有策略-持仓中
        $allstrategy=Db::name('strategy')
            ->alias('str')
            ->field('str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($history_where)
            ->where($strategywhere)
            ->where(['ad.is_del'=>1,'str.status'=>2])
            ->where(['str.is_cancel_order'=>1])
            ->select();
        if($allstrategy){
            //获取每个股票的现价
            $strategyarr=[];
            $cishu=ceil(count($allstrategy)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($allstrategy as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $kk=>$vv){
                    $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
                }
            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $allstrategy[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }

            //所有能看到的会员的T+1策略个数
            $strategy_num1=0;
            //所有能看到的会员的T+5策略个数
            $strategy_num2=0;
            //信用金
            $credit=0;
            //市值
            $market_value=0;
            //策略金额
            $double_value=0;
            //T+1手续费
            $poundage1=0;
            //T+5手续费
            $poundage2=0;
            //递延费
            $defer_value=0;
            //最后盈亏
            $lastRecordMoney=0;
            //浮动盈亏
            $recordMoney=0;
            //红包使用情况
            $redpacket=0;
            foreach ($allstrategy as $k => $v) {
                if($v['strategy_type']==1){
                    $strategy_num1+=1;
                    $poundage1+=$v['all_poundage'];
                }else{
                    $poundage2+=$v['all_poundage'];
                    $strategy_num2+=1;
                }
                $credit+=$v['credit']+$v['credit_add'];
                $market_value+=$v['market_value'];
                $double_value+=$v['double_value'];
                $defer_value+=$v['defer_value'];
                $recordMoney+=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
                if($v['is_packet']==1){
                    $redpacket+=1;
                }
            }
            $strategy['strategy_num1']=$strategy_num1;
            $strategy['strategy_num2']=$strategy_num2;
            $strategy['credit']=$credit;
            $strategy['market_value']=$market_value;
            $strategy['double_value']=$double_value;
            $strategy['poundage1']=$poundage1;
            $strategy['poundage2']=$poundage2;
            $strategy['defer_value']=$defer_value;
            $strategy['lastRecordMoney']=round($lastRecordMoney,2);
            $strategy['recordMoney']=round($recordMoney,2);
            $strategy['redpacket']=$redpacket;
        }else{
            $strategy['strategy_num1']=0;
            $strategy['strategy_num2']=0;
            $strategy['credit']=0;
            $strategy['market_value']=0;
            $strategy['double_value']=0;
            $strategy['poundage1']=0;
            $strategy['poundage2']=0;
            $strategy['defer_value']=0;
            $strategy['lastRecordMoney']=0;
            $strategy['recordMoney']=0;
            $strategy['redpacket']=0;
        }


        //所有能看到的会员的所有策略-已平仓
        $allstrategy_sell=Db::name('strategy')
            ->alias('str')
            ->field('str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($history_where)
            ->where($strategysellwhere)
            ->where(['ad.is_del'=>1,'str.status'=>4])
            ->where(['str.is_cancel_order'=>1])
            ->select();
        if($allstrategy_sell){
            //获取每个股票的现价
            $strategyarr=[];
            $cishu=ceil(count($allstrategy_sell)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($allstrategy_sell as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $arr_sell=explode(';',$result);
                unset($arr_sell[count($arr_sell)-1]);
                foreach($arr_sell as $kk=>$vv){
                    $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
                }
            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $allstrategy_sell[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }

            //所有能看到的会员的T+1策略个数
            $strategy_num1=0;
            //所有能看到的会员的T+5策略个数
            $strategy_num2=0;
            //信用金
            $credit=0;
            //市值
            $market_value=0;
            //策略金额
            $double_value=0;
            //T+1手续费
            $poundage1=0;
            //T+5手续费
            $poundage2=0;
            //递延费
            $defer_value=0;
            //策略盈
            $celueRecordMoney1=0;
            //策略亏
            $celueRecordMoney2=0;
            //最后盈亏--盈利分配
            $lastRecordMoney=0;
            //软件服务费
            $ruanjian=0;
            //浮动盈亏
            $recordMoney=0;
            //红包使用情况
            $redpacket=0;
            foreach ($allstrategy_sell as $k => $v) {
                if($v['strategy_type']==1){
                    $strategy_num1+=1;
                    $poundage1+=$v['all_poundage'];
                }else{
                    $strategy_num2+=1;
                    $poundage2+=$v['all_poundage'];
                }
                $credit+=$v['credit']+$v['credit_add'];
                $market_value+=$v['market_value'];
                $double_value+=$v['double_value'];
                $defer_value+=$v['defer_value'];
                if($v['sell_price']*$v['stock_number']-$v['market_value']>0){
                    $celueRecordMoney1+=$v['sell_price']*$v['stock_number']-$v['market_value'];
                    $lastRecordMoney+=($v['sell_price']*$v['stock_number']-$v['market_value'])*$chengshu;
                    $ruanjian+=($v['sell_price']*$v['stock_number']-$v['market_value'])*(1-$chengshu);
                }else{
                    $celueRecordMoney2+=$v['sell_price']*$v['stock_number']-$v['market_value'];
                }
                if($v['is_packet']==1){
                    $redpacket+=1;
                }
            }
            $strategy_sell['strategy_num1']=$strategy_num1;
            $strategy_sell['strategy_num2']=$strategy_num2;
            $strategy_sell['credit']=$credit;
            $strategy_sell['market_value']=$market_value;
            $strategy_sell['double_value']=$double_value;
            $strategy_sell['poundage1']=$poundage1;
            $strategy_sell['poundage2']=$poundage2;
            $strategy_sell['defer_value']=$defer_value;
            $strategy_sell['celueRecordMoney1']=round($celueRecordMoney1,2);
            $strategy_sell['celueRecordMoney2']=round($celueRecordMoney2,2);
            $strategy_sell['lastRecordMoney']=round($lastRecordMoney,2);
            $strategy_sell['ruanjian']=round($ruanjian,2);
            $strategy_sell['recordMoney']=round($recordMoney,2);
            $strategy_sell['redpacket']=$redpacket;
        }else{
            $strategy_sell['strategy_num1']=0;
            $strategy_sell['strategy_num2']=0;
            $strategy_sell['credit']=0;
            $strategy_sell['market_value']=0;
            $strategy_sell['double_value']=0;
            $strategy_sell['poundage1']=0;
            $strategy_sell['poundage2']=0;
            $strategy_sell['defer_value']=0;
            $strategy_sell['celueRecordMoney1']=0;
            $strategy_sell['celueRecordMoney2']=0;
            $strategy_sell['lastRecordMoney']=0;
            $strategy_sell['ruanjian']=0;
            $strategy_sell['recordMoney']=0;
            $strategy_sell['redpacket']=0;
        }

        //递延策略统计
        $alldiyantrade=Db::name('trade')
            ->alias('tra')
            ->field('tra.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->where($history_where)
            ->where($tradewhere)
            ->where(['tra.trade_status'=>1,'trade_type'=>'扣除递延费'])
            ->where(['ad.is_del'=>1])
            ->where(['tra.is_del'=>1])
            ->select();
        if($alldiyantrade){
            //LRSS 2019-2-25 ADD 重新计算T+1,T+5策略的个数(计算重复)
            $alldiyantrade_list = $alldiyantrade;

            //扣除递延费金额
            $diyanfei=0;
            foreach ($alldiyantrade as $k => $v) {
                $diyanfei+=$v['trade_price'];
            }
            $diyantrade['diyanfei']=round($diyanfei,2);
            //计算策略个数
            //去除相同的
            $alldiyantrade=assoc_unique($alldiyantrade,'charge_num');
            $charge_num='';
            foreach($alldiyantrade as $k=>$v){
                $charge_num.=$v['charge_num'].',';
            }
            $diyanstrategy=Db::name('strategy')->where(['strategy_num'=>['in',$charge_num]])->select();
            $Tonedianyan=0;
            $Tfivedianyan=0;
            foreach($diyanstrategy as $k=>$v){
                if($v['strategy_type']==1){
                    $Tonedianyan+=1;
                }else{
                    $Tfivedianyan+=1;
                }
            }
            $diyantrade['Tonedianyan']=$Tonedianyan;
            $diyantrade['Tfivedianyan']=$Tfivedianyan;

            //LRSS 2019-2-25 ADD 重新计算T+1,T+5策略的个数(计算重复)
            $count_t1 = 0;
            $count_t5 = 0;
            foreach ($alldiyantrade_list AS $k=>$v){
                foreach ($diyanstrategy AS $kk=>$vv){
                    if($v['charge_num'] == $vv['strategy_num']){
                        if($vv['strategy_type']==1){
                            $count_t1 ++;
                        }else{
                            $count_t5 ++;
                        }
                    }
                }
            }
            $diyantrade['Tonedianyan']=$count_t1;
            $diyantrade['Tfivedianyan']=$count_t5;
        }else{
            $diyantrade['diyanfei']=0;
            $diyantrade['Tonedianyan']=0;
            $diyantrade['Tfivedianyan']=0;
        }

        //LRSS 2019-2-25 ADD 获取递延策略列表页面URL
        $info['strategy_type'] = 1;
        $defer_t1_list_url = Url('strategy/deferStrategy',$info);
        $info['strategy_type'] = 2;
        $defer_t5_list_url = Url('strategy/deferStrategy',$info);
        $this->assign('defer_t1_list_url',$defer_t1_list_url);
        $this->assign('defer_t5_list_url',$defer_t5_list_url);

        //交易记录
        $alltrade=Db::name('trade')
            ->alias('tra')
            ->field('tra.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->where($history_where)
            ->where($tradewhere)
            ->where(['tra.trade_status'=>1])
            ->where(['ad.is_del'=>1])
            ->where(['tra.is_del'=>1])
            ->select();
        if($alltrade){
            //连连充值金额
            $recharge_trade_price_lianlian=0;
            //后台充值金额
            $recharge_trade_price_houtai=0;
            //支付宝转账金额
            $recharge_trade_price_zhifubao=0;
            //微信转账金额
            $recharge_trade_price_weixin=0;
            //银行卡转账金额
            $recharge_trade_price_bank=0;
          	//除权派息
            $recharge_trade_price_chuquan=0;
            //已提现金额
            $withdraw_trade_price=0;
            foreach ($alltrade as $k => $v) {
                if($v['trade_type']=='充值'){
                    $recharge_trade_price_lianlian+=$v['trade_price'];
                }elseif($v['trade_type']=='后台充值'){
                    $recharge_trade_price_houtai+=$v['trade_price'];
                }elseif($v['trade_type']=='支付宝转账'){
                    $recharge_trade_price_zhifubao+=$v['trade_price'];
                }elseif($v['trade_type']=='微信转账'){
                    $recharge_trade_price_weixin+=$v['trade_price'];
                }elseif($v['trade_type']=='银行卡转账'){
                    $recharge_trade_price_bank+=$v['trade_price'];
                }elseif($v['trade_type']=='除权派息'){
                    $recharge_trade_price_chuquan+=$v['trade_price'];
                }elseif($v['trade_type']=='提现'){
                    $withdraw_trade_price+=$v['trade_price'];
                }
            }
            $trade['recharge_trade_price_lianlian']=round($recharge_trade_price_lianlian,2);
            $trade['recharge_trade_price_houtai']=round($recharge_trade_price_houtai,2);
            $trade['recharge_trade_price_zhifubao']=round($recharge_trade_price_zhifubao,2);
            $trade['recharge_trade_price_weixin']=round($recharge_trade_price_weixin,2);
            $trade['recharge_trade_price_bank']=round($recharge_trade_price_bank,2);
          	$trade['recharge_trade_price_chuquan']=round($recharge_trade_price_chuquan,2);
            $trade['withdraw_trade_price']=$withdraw_trade_price;
        }else{
            $trade['recharge_trade_price_lianlian']=0;
            $trade['recharge_trade_price_houtai']=0;
            $trade['recharge_trade_price_zhifubao']=0;
            $trade['recharge_trade_price_weixin']=0;
            $trade['recharge_trade_price_bank']=0;
          	$trade['recharge_trade_price_chuquan']=0;
            $trade['withdraw_trade_price']=0;
        }
        //提现申请中金额
        $tradeshenqing=Db::name('trade')
            ->alias('tra')
            ->field('tra.trade_price')
            ->join('link_admin ad','ad.account=tra.account')
            ->where($history_where)
            ->where($tradewhere)
            ->where(['tra.trade_status'=>5,'tra.trade_type'=>'提现'])
            ->where(['ad.is_del'=>1])
            ->where(['tra.is_del'=>1])
            ->select();
        if($tradeshenqing){
            $shenqing=0;
            foreach ($tradeshenqing as $k => $v) {
                $shenqing+=$v['trade_price'];
            }
            $trade['tradeshenqing']=$shenqing;
        }else{
            $trade['tradeshenqing']=0;
        }


        $this->assign('packet',$packet);//红包
        $this->assign('admin',$admin);//会员
        $this->assign('strategy',$strategy);//持仓中策略
        $this->assign('strategy_sell',$strategy_sell);//已平仓策略
        $this->assign('diyantrade',$diyantrade);//递延策略统计
        $this->assign('trade',$trade);//交易记录

        $marketer=Db::name('marketer')->where(['is_del'=>1])->select();
        $this->assign('allmarketer',$marketer);//分销商账户

        return $this->fetch();
    }

    //LRSS 2019-2-27 ADD 导出递延策略
    function exportDeferStrategy(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(cookie('marketer')){
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }else{
            //商户平台管理员
            $where=[];
        }
        $info = Request::instance()->param();

        //账号
        if(isset($info['account'])){
            $where['ad.account'] = $info['account'];
        }

        //分销商
        if(!cookie('marketer') && !cookie('marketer_user')){
            if(isset($info['mark'])){
                $where['ad.marketer_id'] = $info['mark'];
            }
        }

        //时间筛选
        if(isset($info['begin_time']) || isset($info['end_time'])){
            $begin_time = isset($info['begin_time']) ? strtotime($info['begin_time']) : 0;
            $end_time = isset($info['end_time']) ? strtotime($info['end_time']) : 0;

            if($end_time && $end_time < $begin_time){
                $temp_time = 0;
                $temp_time = $end_time;
                $end_time = $begin_time;
                $begin_time = $temp_time;
            }

            if($end_time){
                $end_time = $end_time + 86400;
            }
            if($begin_time && $end_time){
                $where['tra.create_time'][] = array('>=',$begin_time);
                $where['tra.create_time'][] = array('<',$end_time);
            }else{
                if($begin_time){
                    $where['tra.create_time'] = array('>=',$begin_time);
                }
                if($end_time){
                    $where['tra.create_time'] = array('<',$end_time);
                }
            }
        }else{
            if(isset($info['date'])){
                if($info['date'] == 1){
                    //今天
                    $begin_time = strtotime('today');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$begin_time + 86400);
                }
                if($info['date'] == 2){
                    //本周
                    $begin_time = strtotime('this week 00:00:00');
                    $end_time = strtotime('next week 00:00:00');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$end_time);
                }
                if($info['date'] == 3){
                    //本月
                    $begin_time = strtotime('first Day of this month 00:00:00');
                    $end_time = strtotime('first Day of next month 00:00:00');
                    $where['tra.create_time'][] = array('>=',$begin_time);
                    $where['tra.create_time'][] = array('<',$end_time);
                }
                if($info['date'] == 4){
                    //全部
                }
            }
        }

        //获取递延信息
        $defer_res = Db::name('trade')
            ->alias('tra')
            ->field('tra.id AS tra_id,tra.trade_price,tra.create_time,ad.true_name,str.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->join('link_strategy str','str.strategy_num=tra.charge_num')
            ->where(['tra.trade_type'=>'扣除递延费'])
            ->where(['tra.trade_status'=>1])
            ->where(['tra.is_del'=>1])
            ->where(['ad.is_del'=>1])
            ->where($where)
            ->order('tra.id DESC')
            ->select();

        $export_list = array();
        foreach($defer_res as $k=>&$v){
            //策略状态
            if($v['status']==1){
                $v['format_status']='持仓申报中';
            }elseif($v['status']==2){
                $v['format_status']='持仓中';
            }elseif($v['status']==3){
                $v['format_status']='平仓申报中';
            }elseif($v['status']==4){
                $v['format_status']='已平仓';
            }

            //买入时间
            if($v['status']==2||$v['status']==4){
                $v['format_buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
            }

            //卖出时间
            if($v['status']==4){
                $v['format_sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
            }else{
                $v['format_sell_time'] = '';
            }

            //策略类型
            if($v['strategy_type'] == 1){
                $v['format_strategy_type'] = 'T+1';
            }else{
                $v['format_strategy_type'] = 'T+5';
            }

            $v['format_create_time'] = date('Y-m-d H:i:s',$v['create_time']);

            //需要导出的数据
            $temp_list = array(
                'id' => $v['tra_id'], //ID
                'strategy_num' => $v['strategy_num'].' ', //策略单号
                'account' => $v['account'], //用户账号
                'true_name' => $v['true_name'], //真实姓名
                'stock_code' => $v['stock_code'], //股票代码
                'stock_name' => $v['stock_name'], //股票名字
                'credit' => $v['credit'], //信用金
                'double_value' => $v['double_value'], //实际金额
                'format_strategy_type' => $v['format_strategy_type'], //策略种类
                'format_buy_time' => $v['format_buy_time'], //买入时间
                'format_sell_time' => $v['format_sell_time'], //卖出时间
                'defer_day' => $v['defer_day'], //递延天数
                'format_status' => $v['format_status'], //策略状态
                'trade_price' => $v['trade_price'], //递延费
                'format_create_time' => $v['format_create_time'], //缴费时间
            );
            $export_list[] = $temp_list;
        }

        $title='递延策略信息表';
        $cellName = array(
            'ID','策略单号', '用户账号', '真实姓名', '股票代码', '股票名字', '信用金', '实际金额',
            '策略种类', '买入时间', '卖出时间', '递延天数', '策略状态', '递延费', '缴费时间'
        );
        daochu($title,$cellName,$export_list);
    }

    //递延策略导出
    function exportdiyanstrategy(){
        //LRSS 2019-2-27 EDIT
        $this->exportDeferStrategy();
        exit();

        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        $info = Request::instance()->param();

        //1为当天 2为当周 3为当月
        if($info['date']==1){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ymd'))];
            $adminwhere['create_time']=['gt',strtotime(date('Ymd'))];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ymd'))];
        }elseif($info['date']==2){
            $packetwhere['pac.create_time']=['gt',strtotime("this week Monday")];
            $adminwhere['create_time']=['gt',strtotime("this week Monday")];
            $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            $strategysellwhere['str.sell_time']=['gt',strtotime("this week Monday")];
            $tradewhere['tra.create_time']=['gt',strtotime("this week Monday")];
        }elseif($info['date']==3){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ym').'01')];
            $adminwhere['create_time']=['gt',strtotime(date('Ym').'01')];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ym').'01')];
        }else{
            $packetwhere=[];
            $adminwhere=[];
            $strategywhere=[];
            $strategysellwhere=[];
            $tradewhere=[];
        }
        $this->assign('date',$info['date']);

        if(array_key_exists('begin_time',$info)&&$info['begin_time']){
            $packetwhere['pac.create_time']=['gt',strtotime($info['begin_time'])];
            $adminwhere['create_time']=['gt',strtotime($info['begin_time'])];
            $strategywhere['str.buy_time']=['gt',strtotime($info['begin_time'])];
            $strategysellwhere['str.sell_time']=['gt',strtotime($info['begin_time'])];
            $tradewhere['tra.create_time']=['gt',strtotime($info['begin_time'])];
            $this->assign('begin_time',$info['begin_time']);
        }
        if(array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $adminwhere['create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategywhere['str.buy_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategysellwhere['str.sell_time']=['lt',strtotime($info['end_time'])+24*3600];
            $tradewhere['tra.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $this->assign('end_time',$info['end_time']);
        }
        if(array_key_exists('begin_time',$info)&&$info['begin_time']&&array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $adminwhere['create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategywhere['str.buy_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategysellwhere['str.sell_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $tradewhere['tra.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $this->assign('begin_time',$info['begin_time']);
            $this->assign('end_time',$info['end_time']);
        }
        //从会员表过来的时候带的手机号
        if(array_key_exists('account',$info)&&$info['account']){
            $packetwhere['pac.account']=$info['account'];
            $adminwhere['account']=$info['account'];
            $strategywhere['str.account']=$info['account'];
            $strategysellwhere['str.account']=$info['account'];
            $tradewhere['tra.account']=$info['account'];
            $this->assign('account',$info['account']);
        }
        //分销商
        if(array_key_exists('mark',$info)&&$info['mark']){
            $packetwhere['ad.marketer_id']=$info['mark'];
            $adminwhere['marketer_id']=$info['mark'];
            $strategywhere['ad.marketer_id']=$info['mark'];
            $strategysellwhere['ad.marketer_id']=$info['mark'];
            $tradewhere['ad.marketer_id']=$info['mark'];
            $this->assign('markk',$info['mark']);
        }

        //递延策略统计
        $alldiyantrade=Db::name('trade')
            ->alias('tra')
            ->field('tra.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->where($history_where)
            ->where($tradewhere)
            ->where(['tra.trade_status'=>1,'trade_type'=>'扣除递延费'])
            ->where(['ad.is_del'=>1])
            ->where(['tra.is_del'=>1])
            ->select();
        if($alldiyantrade){
            //计算策略个数
            //去除相同的
            $alldiyantrade=assoc_unique($alldiyantrade,'charge_num');
            $charge_num='';
            foreach($alldiyantrade as $k=>$v){
                $charge_num.=$v['charge_num'].',';
            }
            $list=Db::name('strategy')->where(['strategy_num'=>['in',$charge_num]])->select();

            //获取当前单价
            $strategyarr=[];
            $cishu=ceil(count($list)/500);
            for($i=1;$i<=$cishu;$i++){
                foreach($list as $k=>$v){
                    if($k<500*$i&&$k>=500*($i-1)){
                        $strategyarr[$i][]=$v;
                    }
                }
            }
            foreach($strategyarr as $k=>$v){
                $str='';
                foreach($v as $kk=>$vv){
                    $str.=$vv['stock_code'].',';
                }
                $str=substr($str,0,-1);
                $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $kk=>$vv){
                    $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
                }
            }
            foreach($strategyarr as $k=>$v){
                foreach($v as $kk=>$vv){
                    $list[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
                }
            }


            foreach($list as $k=>$v){
                //策略种类  T+1 T+5
                foreach(config('strategy_type') as $kk=>$vv){
                    if($v['strategy_type']==$vv){$list[$k]['strategy_type']=$kk;}
                }
                //已平仓的按照平仓价格计算
                if($v['status']==3||$v['status']==4){
                    //策略盈亏(收益)
                    $list[$k]['income'] = round($v['sell_price']*$v['stock_number']-$v['market_value'],2);
                    if($v['sell_price']*$v['stock_number']-$v['market_value']>0){
                        //赢利分配
                        $list[$k]['lastRecordMoney'] = ($v['sell_price']*$v['stock_number']-$v['market_value'])*$chengshu;
                        //软件服务费
                        $list[$k]['ruanjian'] = ($v['sell_price']*$v['stock_number']-$v['market_value'])*(1-$chengshu);
                    }else{
                        //赢利分配
                        $list[$k]['lastRecordMoney'] = ($v['sell_price']*$v['stock_number']-$v['market_value']);
                        //软件服务费
                        $list[$k]['ruanjian'] = 0;
                    }
                }else{
                    //策略盈亏(收益)
                    $list[$k]['income']='';
                    //赢利分配
                    $list[$k]['lastRecordMoney'] = '';
                    //软件服务费
                    $list[$k]['ruanjian'] = '';
//                //策略盈亏(收益)
//                $list[$k]['income']=round($v['nowprice']*$v['stock_number']-$v['market_value'],2);
//                if($v['nowprice']*$v['stock_number']-$v['market_value']>0){
//                    //赢利分配
//                    $list[$k]['lastRecordMoney'] = ($v['nowprice']*$v['stock_number']-$v['market_value'])*$chengshu;
//                    //软件服务费
//                    $list[$k]['ruanjian'] = ($v['nowprice']*$v['stock_number']-$v['market_value'])*(1-$chengshu);
//                }else{
//                    //赢利分配
//                    $list[$k]['lastRecordMoney'] = ($v['nowprice']*$v['stock_number']-$v['market_value']);
//                    //软件服务费
//                    $list[$k]['ruanjian'] = 0;
//                }
                }
                //策略状态
                if($v['status']==1){
                    $list[$k]['status']='持仓申报中';
                }elseif($v['status']==2){
                    $list[$k]['status']='持仓中';
                }elseif($v['status']==3){
                    $list[$k]['status']='平仓申报中';
                }elseif($v['status']==4){
                    $list[$k]['status']='已平仓';
                }
                //卖出类型
                if($v['sell_type']==1){
                    $list[$k]['sell_type']='主动卖出';
                }elseif($v['sell_type']==2){
                    $list[$k]['sell_type']='达到日期';
                }elseif($v['sell_type']==3){
                    $list[$k]['sell_type']='触发止盈';
                }elseif($v['sell_type']==4){
                    $list[$k]['sell_type']='触发止损';
                }
                //是否进入实盘
                if($v['is_real_disk']==1){
                    $list[$k]['is_real_disk']='是';
                }elseif($v['is_real_disk']==2){
                    $list[$k]['is_real_disk']='否';
                }
                //是否撤单
                if($v['is_cancel_order']==1){
                    $list[$k]['is_cancel_order']='未撤单';
                }elseif($v['is_cancel_order']==2){
                    $list[$k]['is_cancel_order']='撤单中';
                }elseif($v['is_cancel_order']==3){
                    $list[$k]['is_cancel_order']='已撤单';
                }
                //是否使用红包
                if($v['is_packet']==1){
                    $list[$k]['is_packet']='使用';
                }elseif($v['is_packet']==2){
                    $list[$k]['is_packet']='未使用';
                }
                //是否开启自动递延
                if($v['defer']==1){
                    $list[$k]['defer']='开启';
                }elseif($v['defer']==2){
                    $list[$k]['defer']='未开启';
                }
                //买入时间
                if($v['buy_time']){
                    $list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
                }
                //卖出时间
                if($v['sell_time']){
                    $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
                }
            }

            //数字过长可以最后加上空格，防止科学记数法显示
            foreach ($list as $k => $v) {
                $list[$k]['strategy_num']=$v['strategy_num'].' ';
            }
        }else{
            $list=[];
        }

        $title='策略信息表';
        $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
        ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '返还至策略余额的钱'
        ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
        , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
        ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
        ,'卖出时间','真实姓名','现价','策略盈亏','赢利分配','软件服务费');
//        if($info['status']==2){
//            $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
//            ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '返还至策略余额的钱'
//            ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
//            , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
//            ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
//            ,'卖出时间','真实姓名','现价');
//        }elseif($info['status']==4){
//            $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
//            ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '返还至策略余额的钱'
//            ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
//            , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
//            ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
//            ,'卖出时间','真实姓名','现价','策略盈亏','赢利分配','软件服务费');
//        }
        daochu($title,$cellName,$list);
    }

    //策略导出
    function exportstrategy(){
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        $info = Request::instance()->param();

        //1为当天 2为当周 3为当月
        if($info['date']==1){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ymd'))];
            $adminwhere['create_time']=['gt',strtotime(date('Ymd'))];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ymd'))];
        }elseif($info['date']==2){
            $packetwhere['pac.create_time']=['gt',strtotime("this week Monday")];
            $adminwhere['create_time']=['gt',strtotime("this week Monday")];
            $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            $strategysellwhere['str.sell_time']=['gt',strtotime("this week Monday")];
            $tradewhere['tra.create_time']=['gt',strtotime("this week Monday")];
        }elseif($info['date']==3){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ym').'01')];
            $adminwhere['create_time']=['gt',strtotime(date('Ym').'01')];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ym').'01')];
        }else{
            $packetwhere=[];
            $adminwhere=[];
            $strategywhere=[];
            $strategysellwhere=[];
            $tradewhere=[];
        }

        if(array_key_exists('begin_time',$info)&&$info['begin_time']){
            $packetwhere['pac.create_time']=['gt',strtotime($info['begin_time'])];
            $adminwhere['create_time']=['gt',strtotime($info['begin_time'])];
            $strategywhere['str.buy_time']=['gt',strtotime($info['begin_time'])];
            $strategysellwhere['str.sell_time']=['gt',strtotime($info['begin_time'])];
            $tradewhere['tra.create_time']=['gt',strtotime($info['begin_time'])];
        }
        if(array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $adminwhere['create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategywhere['str.buy_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategysellwhere['str.sell_time']=['lt',strtotime($info['end_time'])+24*3600];
            $tradewhere['tra.create_time']=['lt',strtotime($info['end_time'])+24*3600];
        }
        if(array_key_exists('begin_time',$info)&&$info['begin_time']&&array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $adminwhere['create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategywhere['str.buy_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategysellwhere['str.sell_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $tradewhere['tra.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
        }
        //从会员表过来的时候带的手机号
        if(array_key_exists('account',$info)&&$info['account']){
            $packetwhere['pac.account']=$info['account'];
            $adminwhere['account']=$info['account'];
            $strategywhere['str.account']=$info['account'];
            $strategysellwhere['str.account']=$info['account'];
            $tradewhere['tra.account']=$info['account'];
        }
        //分销商
        if(array_key_exists('mark',$info)&&$info['mark']){
            $packetwhere['ad.marketer_id']=$info['mark'];
            $adminwhere['marketer_id']=$info['mark'];
            $strategywhere['ad.marketer_id']=$info['mark'];
            $strategysellwhere['ad.marketer_id']=$info['mark'];
            $tradewhere['ad.marketer_id']=$info['mark'];
        }

        if($info['status']==2){
            //所有能看到的会员的所有策略
            $list=Db::name('strategy')
                ->alias('str')
                ->field('str.id,str.strategy_num,str.account,str.strategy_type,str.status,str.sell_type,str.stock_name,str.stock_code,
                str.stock_number,str.credit,str.credit_add,str.true_getmoney,str.market_value,str.double_value,str.buy_price,str.sell_price,str.surplus_value,
                str.loss_value,str.is_packet,str.is_real_disk,str.shipan_account,str.is_cancel_order,str.buy_contract_num,str.sell_contract_num,
                str.all_poundage,str.buy_poundage,str.sell_poundage,str.defer,str.defer_value,str.defer_day,str.rest_day,str.buy_time,
                str.sell_time,ad.true_name')
                ->join('link_admin ad','ad.account=str.account')
                ->where($history_where)
                ->where($strategywhere)
                ->where(['ad.is_del'=>1,'str.status'=>$info['status']])
                ->where(['str.is_cancel_order'=>1])
                ->select();
        }else{
            //所有能看到的会员的所有策略
            $list=Db::name('strategy')
                ->alias('str')
                ->field('str.id,str.strategy_num,str.account,str.strategy_type,str.status,str.sell_type,str.stock_name,str.stock_code,
                str.stock_number,str.credit,str.credit_add,str.true_getmoney,str.market_value,str.double_value,str.buy_price,str.sell_price,str.surplus_value,
                str.loss_value,str.is_packet,str.is_real_disk,str.shipan_account,str.is_cancel_order,str.buy_contract_num,str.sell_contract_num,
                str.all_poundage,str.buy_poundage,str.sell_poundage,str.defer,str.defer_value,str.defer_day,str.rest_day,str.buy_time,
                str.sell_time,ad.true_name')
                ->join('link_admin ad','ad.account=str.account')
                ->where($history_where)
                ->where($strategysellwhere)
                ->where(['ad.is_del'=>1,'str.status'=>$info['status']])
                ->where(['str.is_cancel_order'=>1])
                ->select();
        }

        //获取当前单价
        $strategyarr=[];
        $cishu=ceil(count($list)/500);
        for($i=1;$i<=$cishu;$i++){
            foreach($list as $k=>$v){
                if($k<500*$i&&$k>=500*($i-1)){
                    $strategyarr[$i][]=$v;
                }
            }
        }
        foreach($strategyarr as $k=>$v){
            $str='';
            foreach($v as $kk=>$vv){
                $str.=$vv['stock_code'].',';
            }
            $str=substr($str,0,-1);
            $result=file_get_contents("http://hq.sinajs.cn/list=".$str);
            $arr=explode(';',$result);
            unset($arr[count($arr)-1]);
            foreach($arr as $kk=>$vv){
                $strategyarr[$k][$kk]['nowprice']=explode(',',$vv)[3];
            }
        }
        foreach($strategyarr as $k=>$v){
            foreach($v as $kk=>$vv){
                $list[$kk+($k-1)*500]['nowprice']=$vv['nowprice'];
            }
        }
      
      
        foreach($list as $k=>$v){
            //策略种类  T+1 T+5
            foreach(config('strategy_type') as $kk=>$vv){
                if($v['strategy_type']==$vv){$list[$k]['strategy_type']=$kk;}
            }
            //已平仓的按照平仓价格计算
            if($v['status']==3||$v['status']==4){
                //策略盈亏(收益)
                $list[$k]['income'] = round($v['sell_price']*$v['stock_number']-$v['market_value'],2);
                if($v['sell_price']*$v['stock_number']-$v['market_value']>0){
                    //赢利分配
                    $list[$k]['lastRecordMoney'] = ($v['sell_price']*$v['stock_number']-$v['market_value'])*$chengshu;
                    //软件服务费
                    $list[$k]['ruanjian'] = ($v['sell_price']*$v['stock_number']-$v['market_value'])*(1-$chengshu);
                }else{
                    //赢利分配
                    $list[$k]['lastRecordMoney'] = ($v['sell_price']*$v['stock_number']-$v['market_value']);
                    //软件服务费
                    $list[$k]['ruanjian'] = 0;
                }
            }else{
                //策略盈亏(收益)
                $list[$k]['income']='';
                //赢利分配
                $list[$k]['lastRecordMoney'] = '';
                //软件服务费
                $list[$k]['ruanjian'] = '';
//                //策略盈亏(收益)
//                $list[$k]['income']=round($v['nowprice']*$v['stock_number']-$v['market_value'],2);
//                if($v['nowprice']*$v['stock_number']-$v['market_value']>0){
//                    //赢利分配
//                    $list[$k]['lastRecordMoney'] = ($v['nowprice']*$v['stock_number']-$v['market_value'])*$chengshu;
//                    //软件服务费
//                    $list[$k]['ruanjian'] = ($v['nowprice']*$v['stock_number']-$v['market_value'])*(1-$chengshu);
//                }else{
//                    //赢利分配
//                    $list[$k]['lastRecordMoney'] = ($v['nowprice']*$v['stock_number']-$v['market_value']);
//                    //软件服务费
//                    $list[$k]['ruanjian'] = 0;
//                }
            }
            //策略状态
            if($v['status']==1){
                $list[$k]['status']='持仓申报中';
            }elseif($v['status']==2){
                $list[$k]['status']='持仓中';
            }elseif($v['status']==3){
                $list[$k]['status']='平仓申报中';
            }elseif($v['status']==4){
                $list[$k]['status']='已平仓';
            }
            //卖出类型
            if($v['sell_type']==1){
                $list[$k]['sell_type']='主动卖出';
            }elseif($v['sell_type']==2){
                $list[$k]['sell_type']='达到日期';
            }elseif($v['sell_type']==3){
                $list[$k]['sell_type']='触发止盈';
            }elseif($v['sell_type']==4){
                $list[$k]['sell_type']='触发止损';
            }
            //是否进入实盘
            if($v['is_real_disk']==1){
                $list[$k]['is_real_disk']='是';
            }elseif($v['is_real_disk']==2){
                $list[$k]['is_real_disk']='否';
            }
            //是否撤单
            if($v['is_cancel_order']==1){
                $list[$k]['is_cancel_order']='未撤单';
            }elseif($v['is_cancel_order']==2){
                $list[$k]['is_cancel_order']='撤单中';
            }elseif($v['is_cancel_order']==3){
                $list[$k]['is_cancel_order']='已撤单';
            }
            //是否使用红包
            if($v['is_packet']==1){
                $list[$k]['is_packet']='使用';
            }elseif($v['is_packet']==2){
                $list[$k]['is_packet']='未使用';
            }
            //是否开启自动递延
            if($v['defer']==1){
                $list[$k]['defer']='开启';
            }elseif($v['defer']==2){
                $list[$k]['defer']='未开启';
            }
            //买入时间
            if($v['buy_time']){
                $list[$k]['buy_time']=date('Y-m-d H:i:s',$v['buy_time']);
            }
            //卖出时间
            if($v['sell_time']){
                $list[$k]['sell_time']=date('Y-m-d H:i:s',$v['sell_time']);
            }
        }

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($list as $k => $v) {
            $list[$k]['strategy_num']=$v['strategy_num'].' ';
        }

        $title='策略信息表';
        $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
        ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '追加的信用金', '返还至策略余额的钱'
        ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
        , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
        ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
        ,'卖出时间','真实姓名','现价','策略盈亏','赢利分配','软件服务费');
//        if($info['status']==2){
//            $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
//            ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '追加的信用金', '返还至策略余额的钱'
//            ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
//            , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
//            ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
//            ,'卖出时间','真实姓名','现价');
//        }elseif($info['status']==4){
//            $cellName = array('ID', '策略单号', '用户账号', '策略种类', '订单状态'
//            ,'卖出类型', '股票名字', '股票代码', '买卖的股数', '信用金', '追加的信用金', '返还至策略余额的钱'
//            ,'市值', '策略金额', '买入价格', '卖出价格', '止盈价格', '止损价格', '是否使用红包'
//            , '是否进入实盘', '实盘账号', '是否撤单', '买入合同编号', '卖出合同编号', '总手续费', '买手续费'
//            ,'卖手续费', '是否开启自动递延', '递延费', '递延天数', '休息天数', '买入时间'
//            ,'卖出时间','真实姓名','现价','策略盈亏','赢利分配','软件服务费');
//        }
        daochu($title,$cellName,$list);
    }

    //订单记录导出
    function exporttrade(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        $info = Request::instance()->param();

        //1为当天 2为当周 3为当月
        if($info['date']==1){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ymd'))];
            $adminwhere['create_time']=['gt',strtotime(date('Ymd'))];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ymd'))];
        }elseif($info['date']==2){
            $packetwhere['pac.create_time']=['gt',strtotime("this week Monday")];
            $adminwhere['create_time']=['gt',strtotime("this week Monday")];
            $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            $strategysellwhere['str.sell_time']=['gt',strtotime("this week Monday")];
            $tradewhere['tra.create_time']=['gt',strtotime("this week Monday")];
        }elseif($info['date']==3){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ym').'01')];
            $adminwhere['create_time']=['gt',strtotime(date('Ym').'01')];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ym').'01')];
        }else{
            $packetwhere=[];
            $adminwhere=[];
            $strategywhere=[];
            $strategysellwhere=[];
            $tradewhere=[];
        }

        if(array_key_exists('begin_time',$info)&&$info['begin_time']){
            $packetwhere['pac.create_time']=['gt',strtotime($info['begin_time'])];
            $adminwhere['create_time']=['gt',strtotime($info['begin_time'])];
            $strategywhere['str.buy_time']=['gt',strtotime($info['begin_time'])];
            $strategysellwhere['str.sell_time']=['gt',strtotime($info['begin_time'])];
            $tradewhere['tra.create_time']=['gt',strtotime($info['begin_time'])];
        }
        if(array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $adminwhere['create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategywhere['str.buy_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategysellwhere['str.sell_time']=['lt',strtotime($info['end_time'])+24*3600];
            $tradewhere['tra.create_time']=['lt',strtotime($info['end_time'])+24*3600];
        }
        if(array_key_exists('begin_time',$info)&&$info['begin_time']&&array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $adminwhere['create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategywhere['str.buy_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategysellwhere['str.sell_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $tradewhere['tra.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
        }
        //从会员表过来的时候带的手机号
        if(array_key_exists('account',$info)&&$info['account']){
            $packetwhere['pac.account']=$info['account'];
            $adminwhere['account']=$info['account'];
            $strategywhere['str.account']=$info['account'];
            $strategysellwhere['str.account']=$info['account'];
            $tradewhere['tra.account']=$info['account'];
        }
        //分销商
        if(array_key_exists('mark',$info)&&$info['mark']){
            $packetwhere['ad.marketer_id']=$info['mark'];
            $adminwhere['marketer_id']=$info['mark'];
            $strategywhere['ad.marketer_id']=$info['mark'];
            $strategysellwhere['ad.marketer_id']=$info['mark'];
            $tradewhere['ad.marketer_id']=$info['mark'];
        }

        //交易记录
        $list=Db::name('trade')
            ->alias('tra')
            ->field('tra.*')
            ->join('link_admin ad','ad.account=tra.account')
            ->where($history_where)
            ->where($tradewhere)
            ->where(['tra.trade_status'=>1])
            ->where(['ad.is_del'=>1])
            ->select();
        foreach($list as $k=>$v){
            if($v['trade_type']=='提现'){
                if($v['trade_status']==1){
                    $list[$k]['trade_status']='提现成功';
                }elseif($v['trade_status']==2){
                    $list[$k]['trade_status']='提现失败';
                }elseif($v['trade_status']==3){
                    $list[$k]['trade_status']='提现中';
                }elseif($v['trade_status']==4){
                    $list[$k]['trade_status']='拒绝提现';
                }elseif($v['trade_status']==5){
                    $list[$k]['trade_status']='提现申请中';
                }
            }else{
                if($v['trade_status']==1){
                    $list[$k]['trade_status']='已付款';
                }elseif($v['trade_status']==2){
                    $list[$k]['trade_status']='未付款';
                }
            }
            //是否删除
            if($v['is_del']==1){
                $list[$k]['is_del']='是';
            }elseif($v['is_del']==2){
                $list[$k]['is_del']='否';
            }
            //交易发生时间
            if($v['create_time']){
                $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            }
        }

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($list as $k => $v) {
            $list[$k]['detailed']=$v['detailed'].' ';
            $list[$k]['charge_num']=$v['charge_num'].' ';
        }

        $title='交易记录信息表';
        $cellName = array('ID', '用户账号', '交易单号', '交易金额', '提现手续费'
        ,'剩余金额', '交易类型', '交易状态', '备注', '排序'
        ,'是否删除', '交易发生时间');
        daochu($title,$cellName,$list);
    }

    //红包记录导出
    function exportpacket(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
            $history_where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
            $history_where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
            $history_where['ad.marketer_id']=cookie('marketer');
        }

        $info = Request::instance()->param();

        //1为当天 2为当周 3为当月
        if($info['date']==1){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ymd'))];
            $adminwhere['create_time']=['gt',strtotime(date('Ymd'))];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ymd'))];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ymd'))];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ymd'))];
        }elseif($info['date']==2){
            $packetwhere['pac.create_time']=['gt',strtotime("this week Monday")];
            $adminwhere['create_time']=['gt',strtotime("this week Monday")];
            $strategywhere['str.buy_time']=['gt',strtotime("this week Monday")];
            $strategysellwhere['str.sell_time']=['gt',strtotime("this week Monday")];
            $tradewhere['tra.create_time']=['gt',strtotime("this week Monday")];
        }elseif($info['date']==3){
            $packetwhere['pac.create_time']=['gt',strtotime(date('Ym').'01')];
            $adminwhere['create_time']=['gt',strtotime(date('Ym').'01')];
            $strategywhere['str.buy_time']=['gt',strtotime(date('Ym').'01')];
            $strategysellwhere['str.sell_time']=['gt',strtotime(date('Ym').'01')];
            $tradewhere['tra.create_time']=['gt',strtotime(date('Ym').'01')];
        }else{
            $packetwhere=[];
            $adminwhere=[];
            $strategywhere=[];
            $strategysellwhere=[];
            $tradewhere=[];
        }

        if(array_key_exists('begin_time',$info)&&$info['begin_time']){
            $packetwhere['pac.create_time']=['gt',strtotime($info['begin_time'])];
            $adminwhere['create_time']=['gt',strtotime($info['begin_time'])];
            $strategywhere['str.buy_time']=['gt',strtotime($info['begin_time'])];
            $strategysellwhere['str.sell_time']=['gt',strtotime($info['begin_time'])];
            $tradewhere['tra.create_time']=['gt',strtotime($info['begin_time'])];
        }
        if(array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $adminwhere['create_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategywhere['str.buy_time']=['lt',strtotime($info['end_time'])+24*3600];
            $strategysellwhere['str.sell_time']=['lt',strtotime($info['end_time'])+24*3600];
            $tradewhere['tra.create_time']=['lt',strtotime($info['end_time'])+24*3600];
        }
        if(array_key_exists('begin_time',$info)&&$info['begin_time']&&array_key_exists('end_time',$info)&&$info['end_time']){
            $packetwhere['pac.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $adminwhere['create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategywhere['str.buy_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $strategysellwhere['str.sell_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
            $tradewhere['tra.create_time']=[['gt',strtotime($info['begin_time'])],['lt',strtotime($info['end_time'])+24*3600]];
        }
        //从会员表过来的时候带的手机号
        if(array_key_exists('account',$info)&&$info['account']){
            $packetwhere['pac.account']=$info['account'];
            $adminwhere['account']=$info['account'];
            $strategywhere['str.account']=$info['account'];
            $strategysellwhere['str.account']=$info['account'];
            $tradewhere['tra.account']=$info['account'];
        }
        //分销商
        if(array_key_exists('mark',$info)&&$info['mark']){
            $packetwhere['ad.marketer_id']=$info['mark'];
            $adminwhere['marketer_id']=$info['mark'];
            $strategywhere['ad.marketer_id']=$info['mark'];
            $strategysellwhere['ad.marketer_id']=$info['mark'];
            $tradewhere['ad.marketer_id']=$info['mark'];
        }

        //所有会员总共得到的红包
        $list=Db::name('packet')
            ->alias('pac')
            ->field('pac.*')
            ->join('link_admin ad','pac.account=ad.account')
            ->where($history_where)
            ->where($packetwhere)
            ->where(['ad.is_del'=>1])
            ->select();

        foreach($list as $k=>$v){
            //是否使用
            if($v['is_use']==1){
                $list[$k]['is_use']='是';
            }elseif($v['is_use']==2){
                $list[$k]['is_use']='否';
            }
            //使用时间
            if($v['use_time']){
                $list[$k]['use_time']=date('Y-m-d H:i:s',$v['use_time']);
            }
            //发出时间
            if($v['create_time']){
                $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
            }
            //到期时间
            if($v['daoqi_time']){
                $list[$k]['daoqi_time']=date('Y-m-d H:i:s',$v['daoqi_time']);
            }
        }

        //数字过长可以最后加上空格，防止科学记数法显示
        foreach ($list as $k => $v) {
            $list[$k]['strategy_num']=$v['strategy_num'].' ';
        }

        $title='红包记录信息表';
        $cellName = array('ID', '策略单号', '会员账号', '红包名字', '红包金额'
        ,'是否使用', '使用时间', '获得时间', '到期时间');
        daochu($title,$cellName,$list);
    }

    //创建会员添加的七天记录
    public function createAdminLine(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
        }else{
            //分销商
            $where['marketer_id']=cookie('marketer');
        }

        $day=strtotime(date('Ymd'));
        $where['create_time']=['egt',$day-6*24*3600];
        $where['is_del']=1;
        $admin=Db::name('admin')
            ->where($where)
            ->select();
        $arr=[
            'day1'=>[],
            'day2'=>[],
            'day3'=>[],
            'day4'=>[],
            'day5'=>[],
            'day6'=>[],
            'day7'=>[],
        ];
        foreach($admin as $k=>$v){
            if($v['create_time']<$day-5*24*3600){
                $arr['day1'][]=$v;
            }elseif($v['create_time']<$day-4*24*3600){
                $arr['day2'][]=$v;
            }elseif($v['create_time']<$day-3*24*3600){
                $arr['day3'][]=$v;
            }elseif($v['create_time']<$day-2*24*3600){
                $arr['day4'][]=$v;
            }elseif($v['create_time']<$day-1*24*3600){
                $arr['day5'][]=$v;
            }elseif($v['create_time']<$day){
                $arr['day6'][]=$v;
            }elseif($v['create_time']<time()){
                $arr['day7'][]=$v;
            }
            $admin[$k]['create_time']=date('m-d',$v['create_time']);
        }
        foreach($arr as $k=>$v){
            if($k=='day1'){
                $list[$k]['time']=date('m-d',$day-6*24*3600);
            }elseif($k=='day2'){
                $list[$k]['time']=date('m-d',$day-5*24*3600);
            }elseif($k=='day3'){
                $list[$k]['time']=date('m-d',$day-4*24*3600);
            }elseif($k=='day4'){
                $list[$k]['time']=date('m-d',$day-3*24*3600);
            }elseif($k=='day5'){
                $list[$k]['time']=date('m-d',$day-2*24*3600);
            }elseif($k=='day6'){
                $list[$k]['time']=date('m-d',$day-1*24*3600);
            }elseif($k=='day7'){
                $list[$k]['time']=date('m-d',$day);
            }
            $list[$k]['num']=count($v);
        }
        return $list;
    }
    //创建策略创建的七天记录
    public function createStrategyLine(){
        if(cookie('marketer_user')){
            //分销商工作人员
            $where['ad.marketer_user']=cookie('marketer_user');
        }elseif(!cookie('marketer')){
            //商户平台管理员
            $where=[];
        }else{
            //分销商
            $where['ad.marketer_id']=cookie('marketer');
        }

        $day=strtotime(date('Ymd'));
        $where['buy_time']=['egt',$day-6*24*3600];
        $where['ad.is_del']=1;

        $admin=Db::name('strategy')
            ->alias('str')
            ->field('str.*')
            ->join('link_admin ad','ad.account=str.account')
            ->where($where)
            ->where(['str.is_cancel_order'=>1])
            ->select();
        $arr=[
            'day1'=>[],
            'day2'=>[],
            'day3'=>[],
            'day4'=>[],
            'day5'=>[],
            'day6'=>[],
            'day7'=>[],
        ];
        foreach($admin as $k=>$v){
            if($v['buy_time']<$day-5*24*3600){
                $arr['day1'][]=$v;
            }elseif($v['buy_time']<$day-4*24*3600){
                $arr['day2'][]=$v;
            }elseif($v['buy_time']<$day-3*24*3600){
                $arr['day3'][]=$v;
            }elseif($v['buy_time']<$day-2*24*3600){
                $arr['day4'][]=$v;
            }elseif($v['buy_time']<$day-1*24*3600){
                $arr['day5'][]=$v;
            }elseif($v['buy_time']<$day){
                $arr['day6'][]=$v;
            }elseif($v['buy_time']<time()){
                $arr['day7'][]=$v;
            }
            $admin[$k]['buy_time']=date('m-d',$v['buy_time']);
        }
        foreach($arr as $k=>$v){
            if($k=='day1'){
                $list[$k]['time']=date('m-d',$day-6*24*3600);
            }elseif($k=='day2'){
                $list[$k]['time']=date('m-d',$day-5*24*3600);
            }elseif($k=='day3'){
                $list[$k]['time']=date('m-d',$day-4*24*3600);
            }elseif($k=='day4'){
                $list[$k]['time']=date('m-d',$day-3*24*3600);
            }elseif($k=='day5'){
                $list[$k]['time']=date('m-d',$day-2*24*3600);
            }elseif($k=='day6'){
                $list[$k]['time']=date('m-d',$day-1*24*3600);
            }elseif($k=='day7'){
                $list[$k]['time']=date('m-d',$day);
            }
            $list[$k]['num']=count($v);
        }
        return $list;
    }
    //系统配置
    public function setup(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            for($i=1;$i<=100;$i++){
                if(array_key_exists('value'.$i,$info)&&$info['value'.$i]){
                    $data['value']=$info['value'.$i];
                    Db::name('hide')->where(['id'=>$i])->update($data);
                }
            }

            $url=Db::name('hide')->where(['id'=>28])->value('value');

            //系统ICO图片
            $file = request()->file('value39');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png,ico'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $value = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                    $data['value']=$url.$value;
                    Db::name('hide')->where(['id'=>39])->update($data);
                }else{
                    echo $file->getError();
                }
            }
            //H5LOGO
            $file = request()->file('value31');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png,ico'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $value = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                    $data['value']=$url.$value;
                    Db::name('hide')->where(['id'=>31])->update($data);
                }else{
                    echo $file->getError();
                }
            }

            //PCLOGO
            $file = request()->file('value32');
            if($file){
                $name1 = $file->getInfo()['name'];
                //中文文件名用此行代码转码
                //$name2=iconv('utf-8','gbk',$name1);
                $move = $file->validate(['ext'=>'jpg,jpeg,png,ico'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
                if($move){
                    $value = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
                    $data['value']=$url.$value;
                    Db::name('hide')->where(['id'=>32])->update($data);
                }else{
                    echo $file->getError();
                }
            }

            $this->success('修改成功','index/setup');
        }
        $list=Db::name('hide')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }
    //系统配置
    public function configure(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            for($i=1;$i<=100;$i++){
                if(array_key_exists('value'.$i,$info)&&$info['value'.$i]){
                    $data['value']=$info['value'.$i];
                    Db::name('hide')->where(['id'=>$i])->update($data);
                }
            }

            $this->success('修改成功','index/configure');
        }
        $list=Db::name('hide')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }
    //系统配置
    public function settings(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            for($i=1;$i<=100;$i++){
                if(array_key_exists('value'.$i,$info)&&$info['value'.$i]){
                    $data['value']=$info['value'.$i];
                    Db::name('hide')->where(['id'=>$i])->update($data);
                }
            }

            $this->success('修改成功','index/settings');
        }
        $list=Db::name('hide')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }

    //修改密码
    public function modify(){
        if (Request::instance()->isPost()){
            $info=Request::instance()->param();

            //用户账号
            $account=cookie('stockaccount');
            $marketer=cookie('marketer');
            $marketer_user=cookie('marketer_user');
            $pwd=$info['pwd'];
            $repwd=$info['repwd'];
            if(!$pwd){
                $this->error("密码不能为空",'index/modify');
            }
            if(!$repwd){
                $this->error("确认密码不能为空",'index/modify');
            }
            if($pwd != $repwd){
                $this->error('两次输入密码不一致');
            }

            if($marketer==0){
                //管理员
                Db::name('user')->where(['account'=>$account])->update(['pwd'=>md5($pwd)]);
            }elseif($marketer_user){
                //分销商工作人员
                Db::name('marketer_user')->where(['id'=>$marketer_user])->update(['pwd'=>md5($pwd)]);
            }else{
                //分销商
                Db::name('marketer')->where(['id'=>$marketer])->update(['pwd'=>md5($pwd)]);
            }

            $this->success('提交成功', 'index/index');
        }
        return $this->fetch();
    }


}
